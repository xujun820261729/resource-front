import Vue from 'vue'
 
// 日期格式化过滤器（2019-12-17）
Vue.filter("formatDate", function formatDate(value) {
  var date = new Date(value);
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  if (month < 10) {
    month = "0" + month;
  }
  if (day < 10) {
    day = "0" + day;
  }
  return `${year}年${month}月${day}日`
});

// 数据没有则显示“-” 有则显示两位小数
Vue.filter("dataFomat", (val) => {
  if(!val) return "-"
  return parseFloat(val).toFixed(2)
})
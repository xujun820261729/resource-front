import BasicLayout from '@/layouts/basic-layout'

const meta = {
  auth: true,
}

const pre = 'test-'

export default {
  path: '/test',
  name: '这是测试页面',
  redirect: {
    name: `${pre}test1`,
  },
  meta,
  component: BasicLayout,
  children: [
    {
      path: '/test/test1',
      name: `${pre}test1`,
      meta: {
        ...meta,
        title: '测试页面1',
        closable: true,
      },
      component: () => import('@/pages/test/test1'),
    },
    {
        path: '/test/test2',
        name: `${pre}test2`,
        meta: {
          ...meta,
          title: '测试页面2',
          closable: true,
        },
        component: () => import('@/pages/test/test2'),
      },
      {
        path: '/test/test3',
        name: `${pre}test3`,
        meta: {
          ...meta,
          title: '测试页面3',
          closable: true,
        },
        component: () => import('@/pages/test/test3'),
      },
  ],
}

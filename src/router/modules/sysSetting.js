import BasicLayout from '@/layouts/basic-layout'

const meta = {
  auth: true,
  cache: true,
}

export default {
  path: '/sysSetting',
  name: 'sysSetting',
  redirect: {
    name: `cusFileMGT`,
  },
  meta,
  component: BasicLayout,
  children: [
    {
      path: `personalCenter`,
      name: `personalCenter`,
      meta: {
        ...meta,
        title: '个人中心',
      },
      component: () =>
        import('@/pages/sysSetting/userCenter/personalCenter.vue'),
    },
    {
      path: `resetPassword`,
      name: `resetPassword`,
      meta: {
        ...meta,
        title: '修改密码',
      },
      component: () =>
        import('@/pages/sysSetting/userCenter/resetPassword.vue'),
    },
  ],
}

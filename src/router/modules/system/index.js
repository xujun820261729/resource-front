import BasicLayout from "@/layouts/basic-layout";

const meta = {
  auth: true,
};

const pre = "system-";

export default {
  path: "/system",
  name: "系统管理",
  redirect: {
    name: `${pre}user`,
  },
  meta,
  component: BasicLayout,
  children: [
    {
      path: "/system/user",
      name: `${pre}user`,
      meta: {
        ...meta,
        title: "用户管理",
        closable: true,
      },
      component: () => import("@/pages/system/user"),
    },
    {
      path: "/system/role",
      name: `${pre}role`,
      meta: {
        ...meta,
        title: "角色管理",
        closable: true,
      },
      component: () => import("@/pages/system/role"),
    },
    {
      path: "/system/chart",
      name: `${pre}chart`,
      meta: {
        ...meta,
        title: "图表维护",
        closable: true,
      },
      component: () => import("@/pages/system/chart"),
    },
  ],
};

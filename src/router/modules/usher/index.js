import BasicLayout from "@/layouts/basic-layout";
const meta = { auth: true };
const pre = "usher-";
export default {
  path: "/usher",
  name: "数据导入",
  redirect: { name: `${pre}substation` },
  meta,
  component: BasicLayout,
  children: [
    {
      path: "/usher/substation",
      name: `${pre}substation`,
      meta: { ...meta, title: "变电站导入", closable: true },
      component: () => import("@/pages/usher/substation"),
    },
    {
      path: "/usher/line",
      name: `${pre}line`,
      meta: { ...meta, title: "线路导入", closable: true },
      component: () => import("@/pages/usher/line"),
    },
    {
      path: "/usher/tower",
      name: `${pre}tower`,
      meta: { ...meta, title: "杆塔导入", closable: true },
      component: () => import("@/pages/usher/tower"),
    },
    {
      path: "/usher/switch",
      name: `${pre}switch`,
      meta: { ...meta, title: "开关导入", closable: true },
      component: () => import("@/pages/usher/switch"),
    },
    {
      path: "/usher/station",
      name: `${pre}station`,
      meta: { ...meta, title: "站所导入", closable: true },
      component: () => import("@/pages/usher/station"),
    },
    {
      path: "/usher/user",
      name: `${pre}user`,
      meta: { ...meta, title: "用户导入", closable: true },
      component: () => import("@/pages/usher/user"),
    },
    {
      path: "/usher/cableSection",
      name: `${pre}cableSection`,
      meta: { ...meta, title: "电缆段导入", closable: true },
      component: () => import("@/pages/usher/cableSection"),
    },
    {
      path: "/usher/boxTypeSubstation",
      name: `${pre}boxTypeSubstation`,
      meta: { ...meta, title: "箱变导入", closable: true },
      component: () => import("@/pages/usher/boxTypeSubstation"),
    },
    {
      path: "/usher/poleUpChange",
      name: `${pre}poleUpChange`,
      meta: { ...meta, title: "杆上变导入", closable: true },
      component: () => import("@/pages/usher/poleUpChange"),
    },
    {
      path: "/usher/rmu",
      name: `${pre}rmu`,
      meta: { ...meta, title: "环网柜导入", closable: true },
      component: () => import("@/pages/usher/rmu"),
    },
    {
      path: "/usher/branchBox",
      name: `${pre}branchBox`,
      meta: { ...meta, title: "分支箱导入", closable: true },
      component: () => import("@/pages/usher/branchBox"),
    },
    {
      path: "/usher/cableWell",
      name: `${pre}cableWell`,
      meta: { ...meta, title: "电缆井导入", closable: true },
      component: () => import("@/pages/usher/cableWell"),
    },

    {
      path: "/usher/kbs",
      name: `${pre}kbs`,
      meta: { ...meta, title: "开闭所导入", closable: true },
      component: () => import("@/pages/usher/kbs"),
    },
  ],
};

import BasicLayout from "@/layouts/basic-layout";
const meta = { auth: true };
const pre = "report-";
export default {
  path: "/report",
  name: "查询报表",
  redirect: { name: `${pre}cableWell` },
  meta,
  component: BasicLayout,
  children: [
    {
      path: "/report/cableWell",
      name: `${pre}cableWell`,
      meta: { ...meta, title: "电缆井统计", closable: true },
      component: () => import("@/pages/report/cableWell"),
    },
    {
      path: "/report/cableChannel",
      name: `${pre}cableChannel`,
      meta: { ...meta, title: "电缆通道统计", closable: true },
      component: () => import("@/pages/report/cableChannel"),
    },
    {
      path: "/report/intermediateJoint",
      name: `${pre}intermediateJoint`,
      meta: { ...meta, title: "图表维护", closable: true },
      component: () => import("@/pages/report/intermediateJoint"),
    },
    {
      path: "/report/cableSection",
      name: `${pre}cableSection`,
      meta: { ...meta, title: "电缆段统计", closable: true },
      component: () => import("@/pages/report/cableSection"),
    },
  ],
};

import BasicLayout from '@/layouts/basic-layout'

const meta = {
  auth: true,
}

const pre = 'userSide-'

export default {
  path: '/userSide',
  name: '用户侧保电应用',
  redirect: {
    path: '/userSide/demandManage',
  },
  meta,
  component: BasicLayout,
  children: [
    {
      path: '/userSide/demandManage',
      name: `${pre}demandManage`,
      meta: {
        ...meta,
        title: '保电需求管理',
        closable: true,
      },
      component: () => import('@/pages/userSide/demandManage'),
    },
    {
      path: '/userSide/resourceBill',
      name: `${pre}resourceBill`,
      meta: {
        ...meta,
        title: '保电资源台账',
        closable: true,
      },
      component: () => import('@/pages/userSide/resourceBill'),
    },
    {
      path: '/userSide/userbill',
      name: `${pre}userbill`,
      meta: {
        ...meta,
        title: '重要用户台账',
        closable: true,
      },
      component: () => import('@/pages/userSide/userbill'),
    },

    {
      path: '/userSide/matchPaly',
      name: `${pre}matchPaly`,
      meta: {
        ...meta,
        title: '赛事看板',
        closable: true,
      },
      component: () => import('@/pages/userSide/matchPaly'),
    },
  ],
}

import BasicLayout from "@/layouts/basic-layout"; // 基础的layout

import system from "./modules/system";
import usher from "./modules/usher";
import report from "./modules/report";

import systehomei from "@/api/system";

/**
 * 在主框架内显示
 */

const frameIn = [
  {
    path: "/",
    name: "index",
    meta: {
      title: "",
      auth: true,
    },
    component: BasicLayout,
    children: [
      {
        path: "log",
        name: "log",
        meta: {
          title: "前端日志",
          auth: true,
        },
        component: () => import("@/pages/system/log"),
      },
      // 刷新页面 必须保留
      {
        path: "refresh",
        name: "refresh",
        hidden: true,
        component: {
          beforeRouteEnter(to, from, next) {
            next((instance) => instance.$router.replace(from.fullPath));
          },
          render: (h) => h(),
        },
      },
      // 页面重定向 必须保留
      {
        path: "redirect/:route*",
        name: "redirect",
        hidden: true,
        component: {
          beforeRouteEnter(to, from, next) {
            next((instance) => instance.$router.replace(JSON.parse(from.params.route)));
          },
          render: (h) => h(),
        },
      },
    ],
  },
  {
    path: "/home",
    name: `home`,
    meta: { auth: true, title: "地图", closable: true },
    component: () => import("@/pages/map"),
  },
  {
    path: "/login",
    name: "login",
    meta: {
      title: "登陆",
    },
    component: () => import("@/pages/login"),
  },

  system,
  usher,
  report,
];

/**
 * 错误页面
 */
const errorPage = [
  {
    path: "/403",
    name: "403",
    meta: {
      title: "403",
    },
    component: () => import("@/pages/system/error/403"),
  },
  {
    path: "/500",
    name: "500",
    meta: {
      title: "500",
    },
    component: () => import("@/pages/system/error/500"),
  },
  {
    path: "*",
    name: "404",
    meta: {
      title: "404",
    },
    component: () => import("@/pages/system/error/404"),
  },
];

// 导出需要显示菜单的
export const frameInRoutes = frameIn;

// 重新组织后导出
export default [...frameIn, ...errorPage];

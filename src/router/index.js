import Vue from "vue";
import VueRouter from "vue-router";
import iView from "view-design";

import util from "@/libs/util";

import Setting from "@/setting";

import store from "@/store/index";

// 路由数据
import routes from "./routes";
Vue.use(VueRouter);

// 解决重复点击路由报错的BUG
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

// 导出路由 在 main.js 里使用
const router = new VueRouter({
  routes,
  mode: Setting.routerMode,
  base: Setting.routerBase,
});

//改变页面的主题(判断是蓝色背景(有新旧两种样式)还是通用白色背景)
function changeTheme(meta) {
  let isPano = (meta || {}).isPano || false;
  // let sysMode = store.state.admin.sysMode.mode.type;
  store.commit("admin/page/setPageType", isPano);
}

router.beforeEach(async (to, from, next) => {
  if (Setting.showProgressBar) iView.LoadingBar.start();
  // 设置背景样式
  changeTheme(to.meta);
  if (to.path !== "/login" && !util.storage.getSharedValue("token")) {
    util.storage.removeSharedValue("userInfo");
    util.storage.removeSharedValue("permissions");
    util.storage.removeSharedValue("menuSider");
    window.location.href = `${window.location.origin}/login`;
    return;
  }
  next();
});

router.afterEach((to) => {
  if (Setting.showProgressBar) iView.LoadingBar.finish();
  // 多页控制 打开新的页面
  store.dispatch("admin/page/open", to);
  // 更改标题
  util.title({
    title: to.meta.title,
  });
  // 返回页面顶端
  window.scrollTo(0, 0);
});

export default router;

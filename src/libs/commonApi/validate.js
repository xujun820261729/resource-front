import { phone,chineseEn } from "@/libs/validator/function"
import { phoneText,requiredText,chineseEnText } from "@/libs/validator/message"
// 验证手机号码 必填
export const validatePhone = (rule, value, callback) => {
  if (!value) {
    return callback(new Error(requiredText));
  } else if (!phone(value)) {
    callback(phoneText);
  } else {
    callback();
  }
};
// 验证手机号码 非必填
export const validatePhoneNR = (rule, value, callback) => {
  if (!value) {
     callback();
  } else if (!phone(value)) {
    callback(phoneText);
  } else {
    callback();
  }
};
// 验证必须是中文或英文
export const chineseEnValidate=(rule, value, callback)=>{
  if (!value) {
    return callback(new Error(requiredText));
  } else if (!chineseEn(value)) {
    callback(chineseEnText);
  } else {
    callback();
  }
}
/**
 * 
 * @param {*} rule 
 * @param {*} value 
 * @param {*} callback 
 * @description 判断时间范围是否为空
 */
export const validateIsNull = (rule, value, callback) => {
  if (value.length===0||!value[0]||!value[1]){
    return callback(new Error(requiredText));
  } 
  callback();
};
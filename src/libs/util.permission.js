import store from "../store"
// 权限检查方法
export function hasPermission(value) {
 let isExist = false;
 let btnPermissions = store.getters["admin/login/permissionList"];
 if (btnPermissions == undefined || btnPermissions == null || btnPermissions.length === 0) {
   return false;
 }
 if (btnPermissions.includes(value)) {
   isExist = true;
 }
 return isExist;
};
import request from "@/plugins/request";

const http = {
  post(url, data = {}, type = "json") {
    return request({
      url,
      method: "post",
      data,
      responseType: type,
    });
  },
  get(url, params, type = "json") {
    return request({
      url,
      method: "get",
      params,
      responseType: type,
    });
  },
  put(url, data, type = "json") {
    return request({
      url,
      method: "put",
      data,
      responseType: type,
    });
  },
  delete(url, params = {}, type = "json") {
    return request({
      url,
      method: "delete",
      params,
      responseType: type,
    });
  },
  upload(
    url,
    formData,
    config = {
      headers: {
        "Content-Type": "multipart/form-data",
        "Access-Control-Allow-Origin": "*",
      },
    }
  ) {
    return request.post(url, formData, config);
  },
};

export default http;

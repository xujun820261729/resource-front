

class webscketHelp {
    ws;
    msg;
    constructor(props) {
        this.init(props)
    }

    init(props) {
        if ("WebSocket" in window) {
            const path = `ws://24.46.125.75:30056/websocket?clientId=${new Date().getTime()}`;
            this.ws = new WebSocket(path);
            this.ws.onmessage = (e) => this.onMessageInfo(e, props);
            this.ws.onopen = this.onOpen;
            this.ws.onerror = this.onError;
            this.ws.onclose = this.onClose;
        } else {
            console.log('浏览器不支持websocket');
        }
    }

    onMessageInfo(info, callback) {
        this.msg = info
        if (info.data === this.msg) {
            console.log('重复');
        } else {
            callback(JSON.parse(info.data))
        }
    }

    onOpen() {
        console.log('util.websocket 已经连上');
    }

    onError(error) {
        console.log('util.websocket 连接错误', error);
    }

    onClose() {
        console.log('util.websocket 连接关闭');
    }

    handlerWsClose() {
        this.ws.close();
    }
}




export default webscketHelp


const requiredText = '该项为必填项';//必填项,不能为空
const maxLengthText = '长度不超过';
const minLengthText = '长度不少于';
const numberText = '必须为有效的数字';
const ipAddrText = '必须为有效的ip地址';
const IdCardText = '必须为有效的身份证';
const emailText = '必须为有效的邮箱';
const phoneText = '必须为有效的电话号码';
const QQPhoneEmailText = '请填写正确的联系方式';//电话，qq，邮箱
const chineseEnText="必须为英文或中文"
export {
  requiredText,
  maxLengthText,
  minLengthText,
  numberText,
  IdCardText,
  emailText,
  phoneText,
  QQPhoneEmailText,
  chineseEnText,
  ipAddrText,
}

import {daysBetween} from "@/libs/commonApi"
// 电话
const phone = (value) => /^1[345789]\d{9}$/.test(value);
// qq
const qq = (value) => /\d{5,11}/.test(value);
// email
const email = (value) => /^\w+@[a-z0-9]+\.[a-z]+$/i.test(value);
// 只能输入中文和英文
 const chineseEn = (value) =>/^[\u4E00-\u9FA5A-Za-z]+$/.test(value)
/**
 * 验证数字类型的 (value 如果等于 空 || undefined || null  一律不走验证 直接返回)
 * @param value 需要验证的值
 * @param length 当前数字总长度
 * @param decimal 当前数字支持小数位数
 * @returns {boolean}
 */
const patternNumber = (value, length = 1, decimal = 0) => {
  if (!value) {
    return false;
  }
  if (length < decimal) {
    console.error('Length cannot be less than decimal places');
    return true;
  }
  let pattern;
  if (decimal) { // 验证小数
    pattern = `(\\s)|(^[0-9]{1,${length - decimal}}$)|(^[0-9]{1,${length - decimal}}\\.([0-9]{1,${decimal}}$))`;
  } else { // 验证整数
    pattern = `(\\s)|(^[0-9]{1,${length}}$)`;
  }
  const str = new RegExp(pattern, 'g');
  return !str.test(value);
};

/**
 * 去掉单个空格
 * @param _params
 * @returns {*}
 */
const replaceStr = (_params) => {
  console.log(_params)
  //判断是否为空
  if(_params === null || _params === undefined){
    return _params
  }else{
    return _params.replace(/\s+/g, '');
  }
};
/**
 * 去掉整个查询参数(对象)的空格
 * @param _params
 * @returns {*}
 */
const replaceParamsStr = (_params) => {
  for (const i in _params) {
    if (_params[i]) {
      // 分页参数是number类型
      _params[i] = _params[i].toString().replace(/\s+/g, '');
    }
  }
  return _params
};



export {
  phone,
  qq,
  email,
  patternNumber,
  replaceStr,
  replaceParamsStr,
  chineseEn
}

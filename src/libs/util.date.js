const date = {
    // 时间差('2020-07-12','2020-07-14',"date" 返回=>["20200712", "20200713", "20200714"])
    dateInterval(beginDate, endDate, type) {
        let dateList = [];
        beginDate = beginDate.replace(/-/g, "")
        beginDate = beginDate.replace(/ /g, "")
        beginDate = beginDate.replace(/:/g, "")
        endDate = endDate.replace(/-/g, "")
        endDate = endDate.replace(/ /g, "")
        endDate = endDate.replace(/:/g, "")

        if (type == 'year') {
            for (beginDate; beginDate <= endDate; beginDate++) {
                dateList.push(String(beginDate))
            }
        } else if (type == 'month') {
            for (Number(beginDate); Number(beginDate) <= Number(endDate);) {
                dateList.push(String(beginDate))
                let year = beginDate.substring(0, 4)
                let month = beginDate.substring(4)
                if (Number(month) < 12) {
                    let m = Number(month) + 1;
                    if (m < 10) {
                        m = '0' + m
                    }
                    beginDate = year + m
                } else if (month == 12) {
                    beginDate = (Number(year) + 1) + '01'
                }
            }
        } else if (type == 'date') {
            for (beginDate; beginDate <= endDate;) {
                dateList.push(beginDate)
                let year = beginDate.substring(0, 4)
                let month = beginDate.substring(4, 6)
                let day = beginDate.substring(6)
                let mDays = new Date(year, month, 0).getDate()

                if (day < mDays) {
                    let d = Number(day) + 1;
                    if (d < 10) {
                        d = '0' + d
                    }
                    beginDate = year + month + d
                } else if (day == mDays) {
                    if (month < 12) {
                        let m = Number(month) + 1;
                        if (m < 10) {
                            m = '0' + m
                        }
                        beginDate = year + m + '01'
                    } else if (month == 12) {
                        beginDate = (Number(year) + 1) + '01' + '01'
                    }
                }
            }
        } else if (type == 'datetime') {
            let reset = (date) => {
                let year = date.substring(0, 4)
                let month = date.substring(4, 6)
                let day = date.substring(6, 8)
                let hours = date.substring(8, 10)
                let minutes = date.substring(10)
                let mDays = new Date(year, month, 0).getDate()

                if (minutes == "00") {} else if (Number(minutes) <= 15) {
                    date = Number(date.substring(0, 10) + "15")
                } else if (Number(minutes) <= 30) {
                    date = Number(date.substring(0, 10) + "30")
                } else if (Number(minutes) <= 45) {
                    date = Number(date.substring(0, 10) + "45")
                } else if (Number(minutes) <= 59) {
                    if (Number(hours) < 23) {
                        let h = Number(hours) + 1;
                        if (h < 10) {
                            h = '0' + hours
                        }
                        date = date.substring(0, 8) + h + "00"
                    } else {
                        if (day < mDays) {
                            let d = Number(day) + 1;
                            if (d < 10) {
                                d = '0' + d
                            }
                            date = year + month + d + "0000"
                        } else {
                            if (month < 12) {
                                let m = Number(month) + 1;
                                if (m < 10) {
                                    m = '0' + m
                                }
                                date = year + m + '01' + "0000"
                            } else {
                                date = (Number(year) + 1) + '01' + '01' + "0000"
                            }
                        }
                    }
                }

                return date
            }


            beginDate = reset(beginDate)
            endDate = reset(endDate)
            for (beginDate; beginDate <= endDate;) {
                beginDate = String(beginDate)
                endDate = String(endDate)
                dateList.push(beginDate)

                let year = beginDate.substring(0, 4)
                let month = beginDate.substring(4, 6)
                let day = beginDate.substring(6, 8)
                let hours = beginDate.substring(8, 10)
                let minutes = beginDate.substring(10)
                let mDays = new Date(year, month, 0).getDate()

                if (minutes == "00") {
                    beginDate = Number(beginDate.substring(0, 10) + "15")
                } else if (Number(minutes) < 15) {
                    beginDate = Number(beginDate.substring(0, 10) + "15")
                } else if (Number(minutes) < 30) {
                    beginDate = Number(beginDate.substring(0, 10) + "30")
                } else if (Number(minutes) < 45) {
                    beginDate = Number(beginDate.substring(0, 10) + "45")
                } else if (Number(minutes) < 59) {
                    if (Number(hours) < 23) {
                        let hour = Number(hours) + 1;
                        if (hour < 10) {
                            hour = '0' + hour
                        }
                        beginDate = beginDate.substring(0, 8) + hour + "00"
                    } else {
                        if (day < mDays) {
                            let d = Number(day) + 1;
                            if (d < 10) {
                                d = '0' + d
                            }
                            beginDate = year + month + d + "0000"
                        } else {
                            if (month < 12) {
                                let m = Number(month) + 1;
                                if (m < 10) {
                                    m = '0' + m
                                }
                                beginDate = year + m + '01' + "0000"
                            } else {
                                beginDate = (Number(year) + 1) + '01' + '01' + "0000"
                            }
                        }
                    }
                }
            }
        }

        dateList = dateList.map(item => {
            if (type == 'month') {
                item = item.substring(0, 4) + "-" + item.substring(4)
            } else if (type == "date") {
                item = item.substring(0, 4) + "-" + item.substring(4, 6) + "-" + item.substring(6)
            } else if (type == "datetime") {
                item = item.substring(0, 4) + "-" + item.substring(4, 6) + "-" + item.substring(6, 8) + " " + item.substring(8, 10) + ":" + item.substring(10)
            }
            return item;
        })

        return dateList;
    },
    // 格式化当前日期
    getNowFormatDate(val, _unit) {
        let date = new Date()

        let seperator = '-'
        //年（2020）
        if (val === 'year') {
            return date.getFullYear() + ''
        }
        //月（2020-05）
        else if (val === 'month') {
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            if (month >= 1 && month <= 9) {
                month = '0' + month
            }
            let currentdate = year + seperator + month
            return currentdate
        }
        //日（2020-05-20）
        else if (val === 'date') {
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let strDate = date.getDate()
            if (month >= 1 && month <= 9) {
                month = '0' + month
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = '0' + strDate
            }
            let currentdate = ''
            if (_unit && _unit === '年月日') {
                currentdate = year + '年' + month + '月' + strDate + '日'
            } else {
                currentdate = year + seperator + month + seperator + strDate
            }
            // console.log(currentdate,'currentdate')
            return currentdate
        }
        //昨天（2020-05-19）/ 一个月前
        else if (val === 'yesterday' || val === 'lastMonth') {
            let day2 = ''
            if (val === 'yesterday') {
                day2 = date.setTime(date.getTime() - 24 * 60 * 60 * 1000);
            } else if (val === 'lastMonth') {
                day2 = date.setTime(date.getTime() - 24 * 60 * 60 * 1000 * 30);
            }
            let currentYear = date.getFullYear();
            let currentMonth = date.getMonth() + 1;
            let yesterday = date.getDate();

            if (currentMonth < 10 && yesterday < 10) {
                return currentYear + "-0" + currentMonth + "-0" + yesterday
            } else if (currentMonth > 10 && yesterday < 10) {
                return currentYear + "-" + currentMonth + "-0" + yesterday
            } else if (currentMonth > 10 && yesterday >= 10) {
                return currentYear + "-" + currentMonth + "-" + yesterday
            } else if (currentMonth < 10 && yesterday >= 10) {
                return currentYear + "-0" + currentMonth + "-" + yesterday
            } else {
                return currentYear + "-" + currentMonth + "-" + yesterday
            }
        }
    },
    // 格式化日期
    formatDate(val, type) {
        let date = val
        let seperator = '-'
        if (type === 'year') {
            return date.getFullYear() + ''
        } else if (type === 'month') {
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            if (month >= 1 && month <= 9) {
                month = '0' + month
            }
            let currentdate = year + seperator + month
            return currentdate
        } else if (type === 'date') {
            let year = date.getFullYear()
            let month = date.getMonth() + 1
            let strDate = date.getDate()
            if (month >= 1 && month <= 9) {
                month = '0' + month
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = '0' + strDate
            }
            let currentdate = year + seperator + month + seperator + strDate
            return currentdate
        }
    },
    //上一个月（2020-04）
    getPreMonth(val) {
        let date = new Date()
        let seperator = '-'
        if (val === 'month') {
            let year = date.getFullYear()
            let month = date.getMonth()
            if (month >= 1 && month <= 9) {
                month = '0' + month
            }
            let currentdate = year + seperator + month
            return currentdate
        }
    },
    formatDateExt(val, fmt) {
        if (typeof (val) == "string") {
            val = new Date(val);
        }
        var o = {
            'M+': val.getMonth() + 1, // 月份
            'd+': val.getDate(), // 日
            'h+': val.getHours(), // 小时
            'm+': val.getMinutes(), // 分
            's+': val.getSeconds(), // 秒
            'q+': Math.floor((val.getMonth() + 3) / 3), // 季度
            S: val.getMilliseconds() // 毫秒
        }
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(
                RegExp.$1,
                (val.getFullYear() + '').substr(4 - RegExp.$1.length)
            )
        }
        for (var k in o) {
            if (new RegExp('(' + k + ')').test(fmt)) {
                fmt = fmt.replace(
                    RegExp.$1,
                    RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)
                )
            }
        }
        return fmt
    },
    //获取当前年
    getCurrentYear() {
        let date = new Date();
        let year = date.getFullYear()
        return `${year}`;
    },
    //获取当前年月
    getCurrentMonth() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() < 9 ? `0${date.getMonth()+1}` : date.getMonth() + 1;
        return `${year}-${month}`;
    },
    //获取星期
    getWeekday() {
        let weekdays = ['日', '一', '二', '三', '四', '五', '六'];
        let week = new Date().getDay();
        let weekday = '星期' + weekdays[week];
        return weekday;
    },
    //获取当前时间点
    getTime() {
        let date = new Date();
        let h = date.getHours() < 9 ? `0${date.getHours()}` : date.getHours();
        let m = date.getMinutes() < 9 ? `0${date.getMinutes()}` : date.getMinutes();
        let s = date.getSeconds() < 9 ? `0${date.getSeconds()}` : date.getSeconds();        
        return h+ ":" + m + ":" + s;
    },
    //获取当前时间
    getCurrentDay() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() < 9 ? `0${date.getMonth()+1}` : date.getMonth() + 1;
        let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        return `${year}-${month}-${day}`;
    },
};

export default date;
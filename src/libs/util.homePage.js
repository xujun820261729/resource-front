
import store from '@/store';
import token from './util.token';
import db from './util.db';
import { pathInit } from '@/store/modules/admin/modules/db';
import { cloneDeep } from 'lodash';

const welcome = "/demand/summary";

//获取子账号的默认主页
function getSubUserHomePage(info){
    let page = "";
    let allRolesHomes = (info || {}).allRolesHomes || [];
    let userInfo = token.initToken();

    console.log('userInfo', userInfo);
    // let groupCode = userInfo.groupCode;
    let customerType = userInfo.customerType;
    if(!customerType){
        return welcome;
    }
    let target = allRolesHomes.find(r => {return r.groupCode && r.groupCode == groupCode});
    if(!target){
        target = allRolesHomes.find(r => {return r.customerType && r.customerType == customerType});
    }
    if(target){
        page = target.functionUri || welcome;
    }else{
        page = welcome;
    }
    return page;
}

const homePage = {
    //获取首页
    getHomePage() {
        let page = "";
        // let isSubUser = !store.getters["admin/subUser/isNull"];
        // let isSwitchUser = !store.getters["admin/switchUser/isNull"];
        // let info = cloneDeep(db.get(pathInit({
        //     dbName: 'sys',
        //     path: 'user.info',
        //     user: true,
        //     defaultValue: {}
        // })).value());
        // if(isSubUser || isSwitchUser){
        //     page =  getSubUserHomePage(info);
        // }else{
        //     page = info.home;
        //     if(!page){
        //         // let groupCode = (store.state.admin.user.userInfo || {}).groupCode;
        //         // if(!groupCode){
        //         //     let tokenInfo = token.initToken() || {};
        //         //     groupCode = tokenInfo.groupCode;
        //         // }
        //         // if(!page) {
        //         //     page = welcome;
        //         // }
        //         page = welcome;
        //     }
        // }
        return welcome;
    },

};

export default homePage;
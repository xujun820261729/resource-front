import moment from 'moment'
import ViewUI from "view-design";

const validate = {};

validate.isNull = function (datas) {
    let msg = []
    datas.map(data => {
        if (data.value === '' || data.value === 'null' || data.value === null || data.value === undefined) {
            msg.push(data.title)
        }
        if (data.value instanceof Array) {
            if (data.value.length === 0) {
                msg.push(data.title)
            }
        }
    })
    if (msg.length > 0) {
        ViewUI.Modal.info({
            title: '提示',
            content: '[ ' + msg.join(' 、') + ' ] 该项是必填项'
        })
        return false
    } else {
        return true
    }
}

validate.checkFiledIsNull = function (filed, errInfo) {
    let flag = true;
    if (typeof (filed) == 'object') {
        filed.forEach((item, index) => {
            if (item == '' || item == 'null' || item == null || item == undefined || item.length == 0) {
                ViewUI.Message.warning(errInfo[index]);
                flag = false;
            }
        })
    } else {
        if (filed == '' || filed == 'null' || filed == null || filed == undefined || filed.length == 0) {
            ViewUI.Message.warning(errInfo);
            flag = false;
        }
    }

    return flag;
}
/**
 * date Array 时间数组  date[0] 开始时间  date[1]  结束时间
 * errInfoArr Array 提示信息
 */
validate.checkoutDaterange = function ({ date, errInfoArr = ['请选择开始时间', '请选择结束时间', '结束时间不能小于开始时间'], format = null }) {

    let isArray = date instanceof Array;
    let flag = true;
    let dateDiff = moment(date[1]).diff(moment(date[0]), 'day');
    let dateDiffone = moment(date[1]).diff(moment(date[0]), 'day');
    if (!isArray) { return false };
    date.map((item, index) => {
        if (!item) {
            ViewUI.Message.warning(errInfoArr[index]);
            flag = false;
        }
    })
    if (date[1] < date[0]) {
        ViewUI.Message.warning(errInfoArr[2]);
        flag = false;
    }
    if (format == 'day' && dateDiff >= 31) {
        ViewUI.Message.warning("由于数据量大时间范围不能大于31天");
        flag = false;
    }
    if (format == 'datetime' && dateDiffone > 1) {
        ViewUI.Message.warning("由于数据量大时间范围不能大于1天");
        flag = false;
    }
    return flag;

}
export default validate;

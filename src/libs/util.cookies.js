import Cookies from 'js-cookie';
import Setting from '@/setting';
import store from '@/store';

const cookies = {};

function cookieInterceptor(type, name, value){
    let sysMode = store.getters["admin/sysMode/mode"];
    if(sysMode == 0){
        return;
    }
    if(type == "set"){
        store.commit("admin/cookies/setCookie", {[name]: value})
    }else{
        let obj = store.getters["admin/cookies/cookies"] || {};
        return obj[name];
    }
}

/**
 * @description 存储 cookie 值
 * @param {String} name cookie name
 * @param {String} value cookie value
 * @param {Object} cookieSetting cookie setting
 */
cookies.set = function (name = 'default', value = '', cookieSetting = {}) {
    let currentCookieSetting = {
        expires: Setting.cookiesExpires
    };
    cookieInterceptor("set", name, value);
    Object.assign(currentCookieSetting, cookieSetting);
    Cookies.set(name, value, currentCookieSetting);
};

/**
 * @description 拿到 cookie 值
 * @param {String} name cookie name
 */
cookies.get = function (name = 'default') {
    let val;
    if(val = cookieInterceptor("get", name)){
        return val;
    }
    return Cookies.get(name);
};

/**
 * @description 拿到 cookie 全部的值
 */
cookies.getAll = function () {
    return Cookies.get();
};

/**
 * @description 删除 cookie
 * @param {String} name cookie name
 */
cookies.remove = function (name = 'default') {
    return Cookies.remove(name);
};

export default cookies;

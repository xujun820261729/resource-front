import dayjs from 'dayjs'
import cookies from './util.cookies'
import log from './util.log'
import db from './util.db'
import validate from './util.validate'
import date from './util.date'
import token from './util.token'
import base64 from './util.base64'
import homePage from './util.homePage'
import downloadFile from './util.downloadFile'
import script from './util.script'
import file from './util.file'
import storage from './sgcc.db'
import http from './http'
import strings from './strings'
import objects from './objects'
import Setting from '@/setting'
import store from '@/store'
import { isEmpty } from "lodash";
import GSscript from "./util.script";


const util = {
  cookies,
  log,
  db,
  validate,
  date,
  token,
  base64,
  homePage,
  downloadFile,
  script,
  file,
  http,
  strings,
  objects,
  storage,
}

function tTitle(title) {
  if (window && window.$t) {
    if (title.indexOf('$t:') === 0) {
      return window.$t(title.split('$t:')[1])
    } else {
      return title
    }
  } else {
    return title
  }
}

/**
 * @description 更改标题
 * @param {Object} title 标题
 * @param {Object} count 未读消息数提示（可视情况选择使用或不使用）
 */
util.title = function ({ title, count }) {
  title = tTitle(title)
  let titleSuffix = Setting.titleSuffix ? `-${Setting.titleSuffix}` : ''
  let fullTitle = title ? `${title}${titleSuffix}` : titleSuffix

  if (count) fullTitle = `(${count}条消息)${fullTitle}`
  window.document.title = fullTitle
}

/**
 * @description 生成随机字符串
 * @param {Object} len 生成的字符串的长度，默认32
 */
util.randomStr = function (len = 32) {
  const $chars =
    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
  const maxPos = $chars.length
  let str = ''
  for (let i = 0; i < len; i++) {
    str += $chars.charAt(Math.floor(Math.random() * maxPos))
  }
  return str
}

/**
 * @description 文件下载
 * @param {Object} blob 文件的二进制流
 * @param {Object} name 文件名，包含后缀名
 */
util.download = function (blob, name) {
  // 通过 blob 对象获取对应的 url
  const url = URL.createObjectURL(blob)
  let a = document.createElement('a')
  a.download = name
  a.style.display = 'none'
  a.href = url
  document.body.appendChild(a)
  a.click()
  a.remove()
}



// 解析url中的search参数并转换成对象
util.parseQueryString = function (url) {
  let regUrl = /^[^\?]+\?([\w\W]+)$/
  let regPara = /([^&=]+)=([\w\W]*?)(&|$|#)/g
  let arrUrl = regUrl.exec(url)
  let ret = {}
  if (arrUrl && arrUrl[1]) {
    let strPara = arrUrl[1]
    let result
    while ((result = regPara.exec(strPara)) != null) {
      ret[result[1]] = result[2]
    }
  }
  return ret
}


function requestAnimation(task) {
  if ('requestAnimationFrame' in window) {
    return window.requestAnimationFrame(task)
  }

  setTimeout(task, 16)
}

/**
 * 时间转换
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
export function parseTime(time, cFormat) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if (typeof time === 'string') {
      if (/^[0-9]+$/.test(time)) {
        time = parseInt(time)
      } else {
        time = time.replace(new RegExp(/-/gm), '/')
      }
    }
    if (typeof time === 'number' && time.toString().length === 10) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay(),
  }
  const timeStr = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    return value.toString().padStart(2, '0')
  })
  return timeStr
}

/**
 * 通过设备id 查出线路
 * @params devIds DeviceIdOptions[]	 {devId:设备ID;devType:设备类型;provinceId:分省ID，总部应用需要传入;distribution:0 配网，1 主网。如果devType没有重复，可以不指定}
 * @description devIds= [{ devId:"eb8cca8d388a06deec50e250090150eb8cc6d90482", devType:"zf01",provinceId:"ff80808149f52e24014a039871840007", distribution:1, }]
*/
export async function mapQueryDeviceByIds(devIds = [
  {
    devId: "6476da94-1601-46e0-ae12-a9df02119855",
    distribution: 1,
    devType: "dxd",
  },
],) {

  return new Promise(async (relove, reject) => {
    const baseUrl = 'http://pms.pro.js.sgcc.com.cn:32100/pms-amap-portal/'
    if (!window.NARIGIS) {
      await GSscript.loadScript(
        baseUrl + "/webgissdk/libs/mapbox/src/mapbox-gl.js"
      );
      await GSscript.loadScript(
        baseUrl + "/webgissdk/libs/webgis/narigis.min.js"
      );

      await GSscript.loadScript(
        baseUrl + "/webgissdk/libs/sgauth/md5.js",
      );

      await GSscript.loadScript(
        baseUrl + "/webgissdk/libs/sgauth/sm3.js",
      );

      const SGMapParams = {
        //服务地址
        serviceUrl: "http://21.47.224.119:21100",
        //应用key
        appKey: "327e636fcd3e498d9024c0728ca28bbe",
        //加密密钥
        secretKey: "8fefc174-155b-4ecb-8114-50e5b778a64e",
      };

      Require(["ResourceQuery",], () => {
        NARIGIS.SGAuth.login(SGMapParams)
          .then(async function (result) {
            if (result.success) {
              console.log("登录成功");
              queryMapDevice(devIds, relove, reject)
            } else {
              console.log("登录失败", result);
            }
          })
          .catch((err) => {
            console.log("错误", err);
          });
      });
    } else {
      queryMapDevice(devIds, relove, reject)
    }

  })




}


const queryMapDevice = async (devIds, relove, reject) => {
  const queryDeviceByIds = new NARIGIS.ResourceQuery.DeviceQuery()
    .queryDeviceByIds;
  await queryDeviceByIds(
    { devIds, maxRecord: 9999 },
    (res) => {
      if (res.isSuccess()) {
        const _json = res.toGeojson().features;
        relove(_json)
      } else {
        console.log("queryDeviceByIds erro ");
        reject(false)
      }
    },
    (err) => {
      console.error(err);
    }
  );

}

/**
 * 手动开启视频调用
 * @list 数据
 *  {
    layout: "1x1",  //  2x2  3x3 4x4 5x5 1x2 1x4 4x6 1+2  1+5  1+5 1+7 1+8 3+4
    className: "video-wrap", // 绑定的外部容器
    talk: "0", // 是否开启语音对讲  0:不开启，1：开启
  }
 * @config 配置
  [
    {
      url: "rtsp://Sxb001:sxb1234!@24.46.27.105:554/cam/realmonitor?channel=4&subtype=1&unicast=true&proto=Onvif",
      deviceCode: "SZIPCSDK3205010050",
    },
  ]
*/
util.handleOpenHKVideo = ({
  config,
  list,
  ...args
}) => {
  store.commit('admin/largeScreen/setState', {
    ocxInfo: {
      config,
      list,
      ...args
    },
  })
}


/**
 * 手动开启视频调用
 * @list 数据
 * @config 配置
*/
util.handleCloseHKVideo = () => {
  store.commit('admin/largeScreen/setState', {
    ocxInfo: {
      ...store.state.admin.largeScreen.ocxInfo,
      list: undefined
    },
  })
}


export { requestAnimation }

export default util

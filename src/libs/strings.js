const strings = {
    isEmpty(string){
        return string === null || string === undefined || (string+"").trim() === '';
    }
}
export default strings;
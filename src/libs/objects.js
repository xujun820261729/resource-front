const objects = {
    copy(src, desc) {
        let i, toStrF = Object.prototype.toString, strC = "[Object Array]";
        desc = desc || {}
        for (i in src) {
            if (src.hasOwnProperty(i)) {
                //复制非对象、非数组的属性
                if ((!this.isObject(src[i]) && !this.isArray(src[i])) || src[i] == null) {
                    desc[i] = src[i]
                } else {
                    //不支持对象或者的复制
                }
            }
        }
        return desc
    },
    clearObjectProperties(jsonObj) {
        if (typeof jsonObj === 'object') {
            let i;
            for (i in jsonObj) {
                if (jsonObj.hasOwnProperty(i)) {
                    jsonObj[i] = null;
                }
            }
        } else {
            console.error("clearObjectProperties 方法只支持对象调用")
        }
    },
    isArray(o) {
        if (typeof Array.isArray === "function") {
            return Array.isArray(o);
        } else {
            return Object.prototype.toString.call(o) === "[object Array]";
        }
    },
    isObject(o) {
        return typeof o === "object";
    }
}
export default objects;
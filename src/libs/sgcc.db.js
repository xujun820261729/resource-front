const storage = {
    //存储
    set(key, value) {
        try {
            sessionStorage.setItem(key, JSON.stringify(value));
        }
        catch (e) {
            console.error("sessionStorage.setItem方法发生异常，存储内容%s=%s", key, value, e);
            sessionStorage.setItem(key, value);
        }
    },
    setSharedValue(key, value) {
        try {
            localStorage.setItem(key, JSON.stringify(value));
        }
        catch (e) {
            console.error("localStorage.setItem方法发生异常，存储内容%s=%s", key, value, e);
            localStorage.setItem(key, value);
        }
    },
    getSharedValue(key) {
        try {
            return JSON.parse(localStorage.getItem(key));
        }
        catch (e) {
            console.error("值不存在或者不是合法的json");
            return localStorage.getItem(key);
        }
    },
    //取出数据
    get(key) {
        try {
            return JSON.parse(sessionStorage.getItem(key));
        }
        catch (e) {
            console.error("值不存在或者不是合法的json");
            return sessionStorage.getItem(key);
        }
    },
    //删除数据
    remove(key) {
        sessionStorage.removeItem(key);
    },
    //删除数据
    removeSharedValue(key) {
        localStorage.removeItem(key);
    }
}
export default storage;
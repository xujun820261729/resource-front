import storage from './sgcc.db';
import store from "@/store";
import { Base64 } from 'js-base64';

const token = {};
let origionTokenInfo, newTokenInfo;

//获取原始token信息
function getOrigionTokenInfo() {
  if (origionTokenInfo) {
    return origionTokenInfo;
  } else {
    let tokenStr = storage.get('token')
    if (!tokenStr || tokenStr == '-1') { return false }
    let userMessage = "";
    try {
      userMessage = Base64.decode(tokenStr.split('.')[1]).replace(/\'/g, '')
    } catch (err) {
      console.log(err);
    }

    userMessage = userMessage.substring(0, userMessage.indexOf("}") + 1)
    userMessage = JSON.parse(userMessage)
    origionTokenInfo = userMessage
    return origionTokenInfo;
  }
}

//获取新的token信息(用于子账号跳转和账号切换的场景)
function getNewTokenInfo(type) {
  if (newTokenInfo) {
    return newTokenInfo;
  }
}

token.initToken = function () {
  let isSubUser = !store.getters["admin/subUser/isNull"];
  let isSwitchUser = !store.getters["admin/switchUser/isNull"];
  if (isSubUser) {
    return getNewTokenInfo("subUser");
  } else if (isSwitchUser) {
    return getNewTokenInfo("switchUser");
  } else {
    return getOrigionTokenInfo();
  }
}

token.clearNewTokenInfo = function () {
  newTokenInfo = null;
}

export default token;

const downloadFile = {};
downloadFile.cutShow = function (flag = false, index = 0, downloadText = 'downloadText', downloadIcon = 'downloadIcon') {
    let text = document.querySelectorAll(`#${downloadText}${index}`);
    let icon = document.querySelectorAll(`#${downloadIcon}${index}`);
    if (!flag) {
        (text || []).forEach(t => {
            t.style.display = "inline-block";
        });
        (icon || []).forEach(i => {
            i.style.display = "none";
        });
    } else {
        (text || []).forEach(t => {
            t.style.display = "none";
        });
        (icon || []).forEach(i => {
            i.style.display = "inline-block";
        });
    }
}
export default downloadFile;
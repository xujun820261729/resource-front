//文件相关的操作类，如下载，预览

const file = {}

/**
 * @description 文件的guid
 */
function guid() {
  function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  }
  return (
    S4() +
    S4() +
    '-' +
    S4() +
    '-' +
    S4() +
    '-' +
    S4() +
    '-' +
    S4() +
    S4() +
    S4()
  )
}

file.guid = guid

/**
 * @description 文件下载
 * @param {Object} blob 文件的二进制流
 * @param {Object} name 文件名，包含后缀名
 */
file.download = function(blob, name) {
  // 通过 blob 对象获取对应的 url
  const url = URL.createObjectURL(blob)
  let a = document.createElement('a')
  a.download = name
  a.style.display = 'none'
  a.href = url
  document.body.appendChild(a)
  a.click()
  a.remove()
}

export default file

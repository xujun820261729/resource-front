import HTMLtoDOCX from 'html-to-docx';
import { saveAs } from 'file-saver';
import * as echarts from 'echarts';
/**
 * echarts 生成 promise
 * @params id 挂在的id
 * @params option echarts参数
 *
 * */
export const loadCharts = (options, transform = false, name) => {
  return new Promise((resolve, reject) => {
    const {
      id,
      XTimes,
      series } = options;

    if (!id) {
      reject('loadCharid  id undefind')
      return
    }
    const myChart = echarts.init(document.getElementById(id));
    // 绘制图表
    myChart.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'none'
        },
      },
      color: ['#2391ff', '#ffc328', '#5ad8a6'],
      legend: {
        top: 10,
        let: 200,
        itemWidth: 10,
        itemHeight: 10,
        textStyle: {
          fontSize: 14,
          padding: [3, 0, 0, 0]
        },
        data: ['缺陷', '隐患', name]
      },
      grid: {
        right: '4%',
        left: '3%',
        bottom: '13%',
        containLabel: true,
      },
      xAxis: {
        type: 'category',
        data: XTimes
      },
      yAxis: {
        type: 'value',
        axisLine: {
          width: 5
        },
        splitLine: {
          show: false
        }
      },
      series: series
    });
    // 渲染结束后替换图片
    // series data 没有数据不会触发 finished 增加 1.5 秒兼容
    let flag = false;
    series.forEach(res => {
      if (res.data.some(e => e)) {
        flag = true
      }
    })

    if (!flag) {
      console.log(id, '发现存在没有数据，兼容1.5秒触发');
      setTimeout(() => {
        if (transform) {
          transformToImage(id)
        }
        resolve(true);
      }, 1500)
    } else {

      myChart.on('finished', () => {
        if (transform) {
          transformToImage(id)
        }
        resolve(true);

      });
    }

  }).catch((error) => {

    console.log(`echarts ${id} catch error`, error);
    reject(false);
  })
};


/**
 *  canvas 转 img
 *
 */
const transformToImage = (id) => {
  const canvasWraDom = window.document.getElementById(id);
  const canvasDom = canvasWraDom.querySelector('canvas');
  const canvasTep = `<img src="${canvasDom.toDataURL()}" style="display: block; width: 350px; height: 150px"  />`;
  canvasWraDom.innerHTML = canvasTep;
}

/**
 * 导出函数
 * @params loading
 *
 */
export const exportDocx = async (htmlTemp, name = test) => {
  const fileBuffer = await HTMLtoDOCX(`<!DOCTYPE html>
  <html lang="en"> 
  <style>\
  .mt5 {
    margin-top: 5px;
  }
  .cus_table {
    border: 1px solid #ccc;
    border-collapse: collapse;
  }
  .cus_table td {
    border: 1px solid #ccc;
    width: 100px;
    text-align: center;
    font-size: 11px;
  }
  </style>      
    ${htmlTemp}
  </html>`, null, {
    table: { row: { cantSplit: true } },
    footer: true,
    pageNumber: true,
  });

  saveAs(fileBuffer, `${name}.docx`);

}


export default exportDocx;

/**
 * @description 鉴权指令
 * 当传入的权限当前用户没有时，会移除该组件
 * 用例：<Tag v-auth>text</Tag>
 *  地图编辑 map:edit
 *  新增角色 system:role:add
 *  修改角色 system:role:edit
 *  删除角色 system:role:delete
 *  停用 system:user:deactivate
 *  新增用户 system:user:add
 *  修改用户 system:user:edit
 *  重置密码  system:user:resetpassword
 * */
import util from "@/libs/util";
import _ from "lodash";

export default {
  inserted(el, binding, vnode) {
    const access = util.storage.getSharedValue("permissions") || [];
    if (!_.isEmpty(access) && !access.includes(binding.value)) {
      el.parentNode && el.parentNode.removeChild(el);
    }
  },
};

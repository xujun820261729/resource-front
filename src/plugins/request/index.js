import axios from "axios";
import util from "@/libs/util";
import Setting from "@/setting";
import store from "@/store/index";
import ViewUI from "view-design";
import CryptoJS from "crypto-js";

const SettingEvn = require("@/setting.env");

// 创建一个错误
function errorCreate(msg) {
  const err = new Error(msg);
  errorLog(err);
  throw err;
}

// 记录和显示错误
function errorLog(err) {
  // 打印到控制台
  if (process.env.NODE_ENV === "development") {
    util.log.error(">>>>>> Error >>>>>>");
    console.log(err);
  }
  // 显示提示，可配置使用 iView 的 $Message 还是 $Notice 组件来显示
  if (Setting.errorModalType === "Message") {
    ViewUI.Message.error({
      content: err.message,
      duration: Setting.modalDuration,
    });
  } else if (Setting.errorModalType === "Notice") {
    ViewUI.Notice.error({
      title: "提示",
      desc: err.message,
      duration: Setting.modalDuration,
    });
  }
}

// 创建一个 axios 实例
const service = axios.create({
  baseURL: "", //接口已经定义了完整的请求地址，这里不需要再配置
  timeout: 2 * 60 * 1000, // 请求超时时间
});

// service.defaults.baseURL = "https://3s155540v7.oicp.vip";

// 请求拦截器
service.interceptors.request.use(
  (config) => {
    // console.log("请求拦截器", config);
    //加密----------------------
    // config.data = CryptoJS.AES.encrypt(
    //   CryptoJS.enc.Utf8.parse(JSON.stringify(config.data)),
    //   CryptoJS.enc.Utf8.parse("c9d2eea8faea11e996ed14ecd17545c4"),
    //   {
    //     mode: CryptoJS.mode.ECB
    //   }
    // ).toString()
    //---------------------------
    ViewUI.LoadingBar.start();

    // 请求状态存入store中
    store.commit("setState", {
      requestUrls: [...store.state.requestUrls, config.url],
    });
    // 在请求发送之前做一些处理
    // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
    config.headers["Authorization"] = util.storage.getSharedValue("token");
    return config;
  },
  (error) => {
    // 发送失败
    Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    ViewUI.LoadingBar.finish();

    // 请求状态存入store中
    store.commit("setState", {
      requestUrls: store.state.requestUrls.filter((url) => url !== response.config.url),
    });
    // dataAxios 是 axios 返回数据中的 data
    const dataAxios = response.data;

    // console.log("响应拦截器", response);
    if (!dataAxios) {
      return response;
    }
    // 这个状态码是和后端约定的
    const { code } = dataAxios;
    // 根据 code 进行判断
    if (code === undefined) {
      // 如果没有 code 代表这不是项目后端开发的接口
      return dataAxios;
    } else {
      // 有 code 代表这是一个后端接口 可以进行进一步的判断
      switch (code) {
        case 200:
          // [ 示例 ] code === 200 代表没有错误
          return dataAxios;
        case 401:
          util.storage.removeSharedValue("userInfo");
          util.storage.removeSharedValue("token");
          util.storage.removeSharedValue("permissions");
          util.storage.removeSharedValue("menuSider");
          window.location.href = `${window.location.origin}/login`;

          // 清理缓存
          errorCreate(`${dataAxios.msg}`);
          break;
        case 418:
          ViewUI.Message.error({
            content: "你操作太快了,慢一点",
            duration: Setting.modalDuration,
          });
          break;
        case 500:
          // [ 示例 ] 其它和后台约定的 code
          errorCreate(dataAxios.msg);
          break;
        case 501:
          ViewUI.Message.warning({
            content: dataAxios.msg,
            duration: Setting.modalDuration,
          });
          break;
        default:
          // 不是正确的 code
          // ViewUI.Message.error({
          //   content: dataAxios.message,
          //   duration: Setting.modalDuration,
          // })

          return dataAxios;
      }
    }
  },
  (error) => {
    ViewUI.LoadingBar.error();
    // 请求状态存入store中

    store.commit("setState", {
      requestUrls: store.state.requestUrls.filter((url) => url !== error.config.url),
    });
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = "请求错误";
          break;
        case 401:
          error.message = "未授权，请登录";
          break;
        case 403:
          if (error.response.data && error.response.data.code == "10001") {
            //游客需要绑定用能客户
            store.commit("guest/openDialog");
            return Promise.reject(error);
          } else {
            error.message = "拒绝访问";
          }
          break;
        case 404:
          error.message = `请求地址出错: ${error.response.config.url}`;
          break;
        case 408:
          error.message = "请求超时";
          break;
        case 500:
          error.message = "服务器内部错误";
          break;
        case 501:
          error.message = "服务未实现";
          break;
        case 502:
          error.message = "网关错误";
          break;
        case 503:
          error.message = "服务不可用";
          break;
        case 504:
          error.message = "网关超时";
          break;
        case 505:
          error.message = "HTTP版本不受支持";
          break;
        default:
          break;
      }
    }
    errorLog(error);
    return Promise.reject(error);
  }
);

export default service;

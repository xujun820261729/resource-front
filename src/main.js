// Vue
import Vue from "vue";
import App from "./App";
import "./assets/styles/animate.css";
// 配置
import Setting from "./setting";
// 混合
import mixinApp from "@/mixins/app";
import query from "@/components/query";

// 插件
import plugins from "@/plugins";
import Viewer from "v-viewer";

// store
import { mapGetters, mapState } from "vuex";

import store from "@/store/index";

// iView 和 iView Pro
import ViewUI from "view-design";
import iViewPro from "@/libs/iview-pro/iview-pro.min.js";

// 菜单和路由
import router from "./router";
import { frameInRoutes } from "@/router/routes";

// 多语言
import i18n from "@/i18n";

// 方法
import cookie from "@/libs/util.cookies.js";
import * as tool from "@/libs/tool";
// 鉴权
import auth from "@/plugins/auth";
Vue.use(Vue.directive("auth", auth));
Vue.prototype.$bus = new Vue();

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

// 内置组件
import iLink from "@/components/link";
import iPagebody from "@/components/pageBody";
import darkTable from "@/components/darkTable";
// 视图组件
// import echarts from 'echarts'
// 使用样式，修改主题可以在 styles 目录下创建新的主题包并修改 iView 默认的 less 变量
// 参考 https://www.iviewui.com/docs/guide/theme

import "./styles/index.less";
import "./libs/iview-pro/iview-pro.css";
import "viewerjs/dist/viewer.css";
import "./assets/styles/base.css";

if (window) window.$t = (key, value) => i18n.t(key, value);

Vue.prototype.$cookie = cookie;
// Vue.prototype.$echarts = echarts
Vue.prototype._ = tool;
Vue.use(plugins);
Vue.use(ViewUI, {
  i18n: (key, value) => i18n.t(key, value),
});
Vue.use(iViewPro);
Vue.use(Viewer);

Vue.component("query", query);
Vue.component("i-link", iLink);
Vue.component("i-pageBody", iPagebody);
Vue.component("dark-table", darkTable);

Vue.filter("noDataFormat", function (value) {
  let data = value + "";
  if (["null", "undefined", "", "NaN"].includes(data)) {
    return " - ";
  } else {
    return value;
  }
});

// 乾坤微应用接入
let instance = new Vue({
  mixins: [mixinApp],
  router,
  store,
  i18n,
  render: (h) => h(App),
  created() {
    // 处理路由 得到每一级的路由设置
    this.$store.commit("admin/page/init", frameInRoutes);
    // 初始化全屏监听
    this.$store.dispatch("admin/layout/listenFullscreen");
  },
  watch: {
    // 监听路由 控制侧边栏显示 标记当前顶栏菜单（如需要）
    $route(to, from) {
      let path = to.matched[to.matched.length - 1].path;
      let menuSider = this.$store.state.admin.menu.menuSider;
      if (Setting.dynamicSiderMenu) {
        let headerName = getHeaderName(path, menuSider);
        if (headerName === null) {
          path = to.path;
          headerName = getHeaderName(path, menuSider);
        }
        // 在 404 时，是没有 headerName 的
        if (headerName !== null) {
          this.$store.commit("admin/menu/setHeaderName", headerName);
          this.$store.commit("admin/menu/setMenuSider", menuSider);

          const filterMenuSider = getMenuSider(menuSider, headerName);
          this.$store.commit("admin/menu/setSider", filterMenuSider);
          this.$store.commit("admin/menu/setActivePath", to.path);

          const openNames = getSiderSubmenu(path, menuSider);
          this.$store.commit("admin/menu/setOpenNames", openNames);
        }
      }
      this.appRouteChange(to, from);
    },
  },
  computed: {
    ...mapGetters("admin/menu", ["currentSiderMenuId"]),
    ...mapState("admin/menu", ["parentList"]),
  },
}).$mount("#app");

import { DataSet, baiduMapLayer, csv } from "mapv";
import { flattenDepth, isEmpty, unionBy } from "lodash";
import "../libs/turf.min.js";
import { utils } from "./utils";
import baseConfig from "../config";
import _ from "lodash";
import systemApi from "@/api/system";

/**
 * 点击事件
 * @that 当前的实例对象
 * @item 当前数据
 *
 */
export const handleIconClick = (that, item, e) => {
  if (that.publicParams.type) {
    console.log("已有弹层打开");
    return;
  }
  console.log("点击事件", item);

  // 1. 如果是线路改变线路颜色
  // if (item.type == "xl") {
  //   let lines = unionBy(
  //     [
  //       {
  //         ...item,
  //         strokeStyle: "#409eff",
  //       },
  //     ],
  //     that.mapLines,
  //     "sub"
  //   );

  //   generateLineAndText(that, lines);
  //   console.log("lines", lines);
  // }

  /**
 数据类型：1:变电站、2:线路、3:杆塔、4:开关、5:站所、6:用户、7:电缆段、8:电缆井
 "9.电缆段导入\n" +
 "10.箱变导入\n" +
 "11.杆上变导入\n" +
 "12.环网柜导入\n" +
 "13.分支箱导入\n" +
  14.中接头
 */
  // 2. 详情弹层 type  1:变电站、2:线路、3:杆塔、4:开关、5:站所、6:用户、7:电缆段、8:电缆井 10:箱变 12: 环网柜; 13:分支箱 11: 开闭所
  const transformKey = {
    8: "bdDlj",
    dlg: "bdDlg",
    7: "bdDlg",
    zjt: "bdZjt",
    1: "bdBdz",
    2: "bdXl",
    3: "bdGt",
    yh: "bdYh",
    5: "bdZSXHF",
    10: "bdZSXHF",
    11: "bdZSXHF",
    12: "bdZSXHF",
    13: "bdZSXHF",
    6: "bdYh",
  };
  if (that.drawingState) {
    console.log("正在绘制中，无法弹出弹层");
  } else {
    that.optionsInfoBox({
      item,
      publicParams: {
        [`${transformKey[item.type]}Visible`]: true,
        params: item, //参数
        type: transformKey[item.type],
      },
      e,
    });
  }
};

/**
 *  地图icon生成
 *  1. icon点位
 *  2. text点位
 *
 */
export const generateIconAndText = (that, points) => {
  if (that.MapVLayers.point) {
    if (!_.isEmpty(points)) {
      that.MapVLayers.point.icon.dataSet.set(
        points.filter((res) => that.showLayers.some((s) => s.type == res.type && s.flag))
      );
      that.MapVLayers.point.text.dataSet.set(
        points.filter((res) => that.showLayers.some((s) => s.type == res.type && s.flag))
      );
    }
  } else {
    // 1. 自定义 icon
    const iconOpt = {
      zIndex: 10003, // 层级
      draw: "icon",
      width: 20,
      mixBlendMode: "overlay",
      height: 20,
      size: 16,
      methods: {
        click: (item, e) => {
          if (item) {
            handleIconClick(that, item, e);
          }
        },
      },
    };
    const textOpt = {
      draw: "text",
      fillStyle: "#8f2eed",
      zIndex: 10003, // 层级
      avoid: true, // 开启文本标注避让
      size: 10,
      offset: {
        // 文本便宜值
        x: 0,
        y: 16,
      },
    };

    const mapSet = new DataSet(points);
    // 自定义icon叠加层
    const MapVIconLayer = new baiduMapLayer(that.map, mapSet, iconOpt);
    const MapVTextLayer = new baiduMapLayer(that.map, mapSet, textOpt);
    // 存储叠加层
    that.MapVLayers.point = {
      icon: MapVIconLayer,
      text: MapVTextLayer,
    };
  }
};

/**
 * 地图显示层控制
 *
 */
const showLayerControl = (that, shows) => {
  if (!_.isEmpty(that.mapPoints)) {
    const filter_points = that.mapPoints.filter((res) => shows.some((s) => s.type == res.type && s.flag));
    generateIconAndText(that, filter_points);
  }
  if (!_.isEmpty(that.mapLines)) {
    const filter_lines = that.mapLines.filter((res) => shows.some((s) => s.type == res.type && s.flag));
    generateLineAndText(that, filter_lines);
  }
};

/**
 * 显示线和名称
 *
 * @data 数据
 *
 */
export const generateLineAndText = (that, data) => {
  // 如果存在 我们仅仅更新数据
  if (that.MapVLayers.line) {
    if (!_.isEmpty(data)) {
      that.MapVLayers.line.dataSet.set(data.filter((res) => that.showLayers.some((s) => s.type == res.type && s.flag)));
    }
  } else {
    // 1. 展示线路 MultiLineString/LineString
    const lineOpt = {
      zIndex: 10001, // 层级
      strokeStyle: "#6b3946",
      lineWidth: 3,
      globalCompositeOperation: "lighter",
      draw: "simple",
      methods: {
        click: (item, e) => {
          if (item) {
            handleIconClick(that, item, e);
          }
        },
        mousemove: (item) => {
          if (item) {
            // console.log("mousemove", item);
          }
        },
      },
    };
    const dataLineSet = new DataSet(data);
    const MapVLineLayer = new baiduMapLayer(that.map, dataLineSet, lineOpt);
    // 存储叠加层
    that.MapVLayers.line = MapVLineLayer;
  }
};

/***
 * 高亮显示线路
 * @data 线路数据
 */
const highShowLine = (that, data) => {
  // 如果存在 我们仅仅更新数据
  if (that.MapVLayers.highLine) {
    let _data = JSON.parse(JSON.stringify(data));
    // 设置默认颜色
    if (!_.isEmpty(_data)) {
      _data.forEach((res) => {
        res.strokeStyle = "#06eed6";
      });
    }
    that.MapVLayers.highLine.dataSet.set(_data);
  } else {
    // 1. 展示线路 MultiLineString/LineString
    const lineOpt = {
      zIndex: 10005, // 层级
      strokeStyle: "#06eed6",
      lineWidth: 5,
      globalCompositeOperation: "lighter",
      draw: "simple",
      methods: {
        click: (item) => {
          if (item) {
            console.log("高亮显示线路 点击触发");
          }
        },
      },
    };
    const dataLineSet = new DataSet(data);
    const MapVLineLayer = new baiduMapLayer(that.map, dataLineSet, lineOpt);
    // 存储叠加层
    that.MapVLayers.highLine = MapVLineLayer;

    that.setState({
      highLineData: data,
    });
  }
};

/***
 * 高亮显示点
 * @data 点的数据
 */
const highShowPoints = (that, data) => {
  let _data = JSON.parse(JSON.stringify(data));
  if (!_.isEmpty(_data)) {
    _data.forEach((res) => {
      res.icon =
        "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB0PSIxNjczMjc5MjY5OTAwIiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjE1MjM0IiBpZD0ibXhfbl8xNjczMjc5MjY5OTAyIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgd2lkdGg9IjIwMCIgaGVpZ2h0PSIyMDAiPjxwYXRoIGQ9Ik01MTIgNDIuNjY2NjY3QzMwMC44IDQyLjY2NjY2NyAxMjggMjE1LjQ2NjY2NyAxMjggNDI2LjY2NjY2N2MwIDM2Mi42NjY2NjcgMzQ5Ljg2NjY2NyA1NDEuODY2NjY3IDM2NC44IDU1MC40IDYuNCAyLjEzMzMzMyAxMi44IDQuMjY2NjY3IDE5LjIgNC4yNjY2NjZzMTIuOC0yLjEzMzMzMyAxOS4yLTQuMjY2NjY2QzU0Ni4xMzMzMzMgOTY4LjUzMzMzMyA4OTYgNzg5LjMzMzMzMyA4OTYgNDI2LjY2NjY2N2MwLTIxMS4yLTE3Mi44LTM4NC0zODQtMzg0eiBtMCA1MTQuMTMzMzMzYy03MC40IDAtMTI4LTU3LjYtMTI4LTEyOHM1Ny42LTEyOCAxMjgtMTI4IDEyOCA1Ny42IDEyOCAxMjgtNTcuNiAxMjgtMTI4IDEyOHoiIGZpbGw9IiNmNTM4MjAiIHAtaWQ9IjE1MjM1Ij48L3BhdGg+PC9zdmc+";
    });
  }

  if (that.MapVLayers.highPoints) {
    that.MapVLayers.highPoints.dataSet.set(_data);
  } else {
    const iconOpt = {
      zIndex: 10006, // 层级
      draw: "icon",
      width: 15,
      mixBlendMode: "overlay",
      height: 15,
      // size: 16,
      methods: {
        click: (item) => {
          if (item) {
            console.log("高亮显示点");
          }
        },
      },
      offset: {
        // 文本便宜值
        x: 0,
        y: -16,
      },
    };
    const mapSet = new DataSet(_data);
    const MapVIconLayer = new baiduMapLayer(that.map, mapSet, iconOpt);
    that.MapVLayers.highPoints = MapVIconLayer;

    that.setState({
      highPoints: _data,
    });
  }
};

/**
 * 地图初始化
 *
 */
const initMap = (that) => {
  utils.printLog("init");
  window.BMap = BMap;
  that.map = new BMap.Map("map-wrap", {
    minZoom: baseConfig.minZoom,
    maxZoom: baseConfig.maxZoom,
    backgroundColor: [192, 214, 213, 100],
  });
  const point = new BMap.Point(baseConfig.centerPoint[0], baseConfig.centerPoint[1]); // 创建点坐标

  that.setState({
    realTimeCoordinates: {
      lng: baseConfig.centerPoint[0],
      lat: baseConfig.centerPoint[1],
    },
  });
  that.map.centerAndZoom(point, baseConfig.minZoom); // 初始化地图，设置中心点坐标和地图级别
  that.map.enableScrollWheelZoom(true); //开启鼠标滚轮缩放
  that.map.addEventListener("click", function (e) {
    // 获取坐标
    // console.log("点击位置经纬度：", e.point.lng, e.point.lat);
    that.tipBox = undefined;
    that.moveBox = undefined;
    that.map.removeOverlay(that.marker);
  });

  that.map.addEventListener("mousemove", function (e) {
    // 经纬度实时更新
    that.setState({
      realTimeCoordinates: e.point,
    });
  });

  that.map.addEventListener("tilesloaded", function () {
    that.tipBox = undefined;
    that.moveBox = undefined;
    that.map.removeOverlay(that.marker);
    utils.printLog("load");

    // 加载所有的点位// 线
    if (!that.mapLoad) {
      getAll_line_point(that);
    }

    that.setState({
      BDMap: that,
      mapLoad: true,
    });
  });
  // 设置地图可拖拽的范围 setBounds
  that._areaRestriction();
};

/**
 * 请求所有的点和线
 *
 */
const getAll_line_point = (that) => {
  // console.log("加载所有点!!!");
  Promise.all([systemApi.getLineByOrgCode(), systemApi.getPointByOrgCode()]).then((res) => {
    const [lines, points] = res;
    if (!_.isEmpty(lines) || !_.isEmpty(points)) {
      // console.log("加载所有的点线");
      let { _ls, _ps } = cutApartData(that, [...lines, ...points]);
      setMapData(that, _ls, _ps);
    }
  });
};

/**
 * 分割出线和点数据
 *
 */
const cutApartData = (that, list, order_obj = {}) => {
  if (_.isEmpty(list)) {
    console.log("cutApartData: 数据为空");
    return;
  }
  // 1-1. 渲染点
  let _ps = [];
  // 1-2. 渲染线路
  let _ls = [];
  list.forEach((res) => {
    let iconObj = that.iconEnumObj[res.type];

    // type 1:变电站、2:线路、3:杆塔、4:开关、5:站所、6:用户、7:电缆段、8:电缆井 10:箱变 11:开闭所 12: 环网柜; 13:分支箱
    switch (res.type) {
      case 2:
        _ls.push({
          geometry: {
            type: "LineString",
            coordinates: [
              [res.startLon, res.startLat],
              [res.endLon, res.endLat],
            ],
          },
          text: res.name,
          type: res.type,
          id: res.id,
          equName: iconObj.equName,
          strokeStyle: iconObj.color,
          lineWidth: iconObj.width,
          ...order_obj,
        });
        break;

      case 7:
        _ls.push({
          geometry: {
            type: "LineString",
            coordinates: [
              [res.startLon, res.startLat],
              [res.endLon, res.endLat],
            ],
          },
          text: res.name,
          type: res.type,
          strokeStyle: iconObj.color,
          lineWidth: iconObj.width,
          id: res.id,
          equName: iconObj.equName,
          ...order_obj,
        });
        break;
      case 99:
        if (res.startLon && res.startLat && res.endLon && res.endLat) {
          _ls.push({
            geometry: {
              type: "LineString",
              coordinates: [
                [res.startLon, res.startLat],
                [res.endLon, res.endLat],
              ],
            },
            strokeStyle: iconObj.color,
            lineWidth: iconObj.width,
            text: res.name,
            type: res.type,
            id: res.id,
            equName: "架空线",
            ...order_obj,
          });
        }
        break;
      default:
        // 5:站所 10:箱变 12: 环网柜; 13:分支箱 使用 站所的图标
        if ([5, 10, 11, 12, 13].includes(res.type)) {
          iconObj = that.iconEnumObj[5];
        }

        if (!iconObj) {
          console.log("iconObj  没有", iconObj, "type", res.type);
          return;
        }

        _ps.push({
          geometry: { type: "Point", coordinates: [res.startLon, res.startLat] },
          text: res.name,
          icon: iconObj.url,
          equName: baseConfig.equipmentObj[`${res.type}`],
          type: res.type,
          id: res.id,
          ...order_obj,
        });
        break;
    }
  });

  // 1-3. 定位
  let allPoints = _.flattenDepth(
    [
      _ps.map((res) => res.geometry.coordinates),
      _.flattenDepth(
        _ls.map((res) => res.geometry.coordinates),
        1
      ),
    ],
    1
  );

  return {
    _ps,
    _ls,
    allPoints,
  };
};
/**
 * 合并数据 a
 *
 */

const mergeMapOrgSet = (org, cur) => {
  if (_.isEmpty(org) || _.isEmpty(cur)) {
    return;
  }

  let mergeList = JSON.parse(JSON.stringify(org));
  let mergeAddList = [];
  cur.forEach((c) => {
    let flag = true;
    mergeList.forEach((o) => {
      // 如果存在相同的数据处理
      if (`${o.id}-${o.type}` == `${c.id}-${c.type}`) {
        // console.log("相同点 mergeMapOrgSet 当前", c);
        // console.log("相同点 mergeMapOrgSet 原", o);
        // 编辑 仅仅只能编辑 1.点坐标 2.名称
        o.equName = c.equName;
        o.geometry = c.geometry;
        o.text = c.text;

        // 是否存在删除
        if (c.delete) {
          o.delete = c.delete;
        }

        flag = false;
      }
    });

    if (flag) {
      mergeAddList.push(c);
    }
  });

  if (!_.isEmpty(mergeAddList)) {
    console.log("新增数据");
    mergeAddList.forEach((a) => {
      mergeList.push(a);
    });
  }

  // 删除 元素
  mergeList = mergeList.filter((r) => !r.delete);

  console.log("mergeMapOrgSet", mergeList);
  return {
    mergeList,
    mergeAddList,
  };
};

/**
 * 新增 / 更新数据/ 删除数据
 */
const setMapData = (that, mapLines = [], mapPoints = []) => {
  // console.log("原mapLines", that.mapLines);
  // console.log("新mapLines", mapLines);

  // console.log("原mapPoints", that.mapPoints);
  // console.log("新mapPoints", mapPoints);

  let mergeMapLines = false;
  let mergeMapAddLines = [];

  const orgLineFlag = _.isEmpty(that.mapLines);
  const curLineFlag = _.isEmpty(mapLines);

  // 1.1 新增
  if (orgLineFlag && !curLineFlag) {
    // 1.1.1 原数据:空 新数据:有
    mergeMapLines = mapLines;
  } else if (!orgLineFlag && !curLineFlag) {
    utils.printLog("setMapData", "更新地图线数据");
    // 1.1.2 原数据:有 新数据:有; 1.1.3 删除的每条数据中存在delete:true
    mergeMapLines = that.mapLines;
    let { mergeList, mergeAddList } = mergeMapOrgSet(mergeMapLines, mapLines);
    mergeMapLines = mergeList;
    mergeMapAddLines = mergeAddList;

    // console.log("出去的line数据-----", mergeMapLines);
  }

  let mergeMapPoints = false;
  let mergeMapAddPoints = [];

  const orgPointFlag = _.isEmpty(that.mapPoints);
  const curPointFlag = _.isEmpty(mapPoints);
  // 1.2 点没有数据
  if (orgPointFlag && !curPointFlag) {
    // 1.1.1 原数据:空 新数据:有
    mergeMapPoints = mapPoints;
  } else if (!orgPointFlag && !curPointFlag) {
    utils.printLog("setMapData", "更新地图点数据");
    // 1.1.2 原数据:有 新数据:有; 1.1.3 删除的每条数据中存在delete:true
    mergeMapPoints = that.mapPoints;
    let { mergeList, mergeAddList } = mergeMapOrgSet(mergeMapPoints, mapPoints);
    mergeMapPoints = mergeList;
    mergeMapAddPoints = mergeAddList;
    // console.log("出去的point数据-----", mergeMapPoints);

    // let _filter = mergeMapPoints.filter((r) => `${r.id}-${r.type}` == "2-3");

    // console.log("出去的point_filter-----", _filter);
  }

  // console.log("mergeMapAddLines", mergeMapAddLines);

  // console.log("mergeMapAddPoints", mergeMapAddPoints);

  // 新增的我们打标记替换老标数据
  highShowLine(that, mergeMapAddLines);
  highShowPoints(that, mergeMapAddPoints);

  let update_obj = {
    mapLines: mergeMapLines,
    mapPoints: mergeMapPoints,
  };

  // 地图视角设置
  if (!_.isEmpty(mergeMapAddLines) || !_.isEmpty(mergeMapAddPoints)) {
    let allPoints = _.flattenDepth(
      [
        mergeMapAddPoints.map((res) => res.geometry.coordinates),
        _.flattenDepth(
          mergeMapAddLines.map((res) => res.geometry.coordinates),
          1
        ),
      ],
      1
    );

    that.fitBounds(allPoints);
  }

  // 初次渲染地图
  if (orgLineFlag && orgPointFlag) {
    // 如果是初始化?
    let unionTypes = _.unionBy(
      [...that.mapLines, ...mapLines, ...mapPoints, ...that.mapPoints].map((res) => ({
        type: res.type,
        equName: res.equName,
        flag: true,
      })),
      "type"
    );
    update_obj = {
      ...update_obj,
      showLayers: unionTypes,
    };

    utils.printLog("setMapData", "初始化地图");
  }

  that.setState(update_obj);
  // 初始化数据/刷新数据
  generateIconAndText(that, mergeMapPoints);
  generateLineAndText(that, mergeMapLines);
};

const core = {
  handleIconClick,
  generateIconAndText,
  showLayerControl,
  generateLineAndText,
  highShowLine,
  highShowPoints,
  initMap,
  setMapData,
  getAll_line_point,
  cutApartData,
};

export default core;

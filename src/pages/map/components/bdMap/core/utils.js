import { maxBy, minBy } from "lodash";
import config from "../config";

// 1.日志类型配置
const logTypeEnum = {
  init: {
    text: "初始化地图",
    level: 4,
    style: "background:#c5c8ce;color:#222",
  },
  setMapData: {
    text: "数据更新",
    level: 3,
    style: "background:#52f6d4;color:#222",
  },
  initPointLins: {
    text: "数据刷新",
    level: 3,
    style: "background:#19be6b96;color:#222",
  },
  load: {
    text: "地图加载完成",
    level: 1,
    style: "background:#ff990096;color:#222",
  },
  areaRestriction: {
    text: "显示区域保护已加载!",
    level: 4,
    style: "background:#c5c8ce;color:#222",
  },
  options: {
    text: "地图事件触发",
    level: 3,
    style: "background:#2db7f5b3;color:#222",
  },
  error: {
    text: "",
    level: 2,
    style: "background:#d53838;color:#fff",
  },
  testDataLoad: {
    text: "触发测试数据加载",
    level: 4,
    style: "background:#c5c8ce;color:#222",
  },
};

// 2.日志等级
let logLevel = 3;

// 3. 日志类型
export const utils = {
  printLog: (type, text) => {
    const typeEnum = logTypeEnum[type];
    if (typeEnum.level <= logLevel) {
      console.info(`%c${type}: %s`, typeEnum.style, text || typeEnum.text);
    }
  },
};

// 计算东南角和西北角的经纬度
export function getCenterBounds(arr, opt = { lat: "lat", lng: "lng", isArr: true }) {
  const { lat, lng, isArr } = opt;

  try {
    if (arr.length == 1) {
      let minLng = arr[0];
      // console.log("center minLng=", minLng);
      let center = [[isArr ? minLng[0] : minLng[lng], isArr ? minLng[1] : minLng[lat]]];
      // console.log("center =", center);
      return center;
    } else if (arr.length == 0) {
      // console.log("center =", arr.length);
      return [];
    } else {
      let minLng = minBy(arr, function (o) {
        return isArr ? o[0] : o[lng];
      });
      let minLat = minBy(arr, function (o) {
        return isArr ? o[1] : o[lat];
      });
      let maxLng = maxBy(arr, function (o) {
        return isArr ? o[0] : o[lng];
      });
      let maxLat = maxBy(arr, function (o) {
        return isArr ? o[1] : o[lat];
      });
      //范围框的西南角。
      let sw = [isArr ? minLng[0] : minLng[lng], isArr ? minLat[1] : minLat[lat]];
      //范围框的东北角。
      let ne = [isArr ? maxLng[0] : maxLng[lng], isArr ? maxLat[1] : maxLat[lat]];
      return [sw, ne];
    }
  } catch (error) {
    console.log("center error=", error);
    return [];
  }
}

// 计算当前的宽高
export function getPointXY(point, that) {
  const TH = 15;

  var poi = new BMap.Point(point.lng, point.lat);
  const { x, y } = that.map.pointToPixel(poi);

  const winH = window.innerHeight; //

  const winW = window.innerWidth; //

  const { height, width } = config.tipBox;

  let _y = 0; // 默认显示在上面

  let _x = 0; // 默认显示在中间

  // 点击的逻辑处理 判断弹层的位置
  let direction_class;

  // 如果底部距离
  // 左侧
  if (x >= width / 2 && winW - x > width / 2) {
    _x = x - width / 2;
    if (y >= height) {
      console.log("1-1");
      _y = y - height - TH;
      direction_class = "top";
    } else {
      console.log("1-2");
      _y = y + TH;
      direction_class = "bottom";
    }
  } else if (x >= width / 2 && winW - x < width / 2) {
    _x = x - width - TH;

    if (y < height / 2) {
      console.log("2-1");
      direction_class = "top-right";
      _y = y;
    } else if (y > height / 2 && winH - y < height / 2) {
      console.log("2-2");
      direction_class = "bottom-right";
      _y = y - height;
    } else {
      console.log("2-3");
      _y = y - height / 2;
      direction_class = "right";
    }
  } else {
    _x = x + TH;
    if (y < height / 2) {
      console.log("3-1");
      direction_class = "top-left";
      _y = y;
    } else if (y > height / 2 && winH - y < height / 2) {
      console.log("3-2");
      _y = y - height;
      direction_class = "bottom-left";
    } else {
      console.log("3-3");
      _y = y - height / 2;
      direction_class = "left";
    }
  }

  console.log("end ----", _x, _y);

  console.log("end ---- direction", direction_class);

  return {
    x: _x,
    y: _y,
    direction_class,
  };
}

const modals = require.context("../modals", true, /\.vue$/);

const outputModules = {};

modals.keys().forEach((key) => {
  outputModules[`${key.split("/")[1]}`] = modals(key).default;
});

export default outputModules;

import Vue from "vue";
import Vuex from "vuex";

import admin from "./modules/admin";
import layoutStyle from "./modules/layoutStyle"; // 是否出现蓝色背景
import message from "./modules/message"; // 是否显示消息弹窗

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    requestUrls: [],
  },
  mutations: {
    /**
     *  纯函数修改state数据
     *  @params  Object
     */
    setState(state, params) {
      Object.entries(params).forEach(([key, value]) => {
        state[`${key}`] = value;
      });
    },
  },
  modules: {
    admin,
    layoutStyle,
    message,
  },
});

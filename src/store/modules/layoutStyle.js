//设置蓝色背景
const state = {
  showBlueLayout: false,
  showBlueLayoutNew: false
}
//get
const getters = {
  //旧的蓝色背景
  getShowBlueLayout: state => {
    return state.showBlueLayout
  },
  //新的蓝色背景
  getShowBlueLayoutNew: state => {
    return state.showBlueLayoutNew
  }
}
//set
const mutations = {
  //旧的蓝色背景
  setShowBlueLayout: (state, showBlueLayout) => {
    state.showBlueLayout = showBlueLayout
  },
  //新的蓝色背景
  setShowBlueLayoutNew: (state, showBlueLayoutNew) => {
    state.showBlueLayoutNew = showBlueLayoutNew
  }
}

export default {
  state,
  getters,
  mutations
}

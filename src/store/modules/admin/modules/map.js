export default {
  namespaced: true,
  state: {
    BDMap: undefined, // 地图对象
    BDTree: undefined, // 树对象
    fullScreen: false, //  地图是否全屏
    publicParams: {
      // bdDljVisible: true,
      // params: {
      //   orgs: {
      //     label: "测试AAA杆塔",
      //   },
      // }, //参数
      // type: "bdDlj",
    },
    showLayers: [], // 显示的图层
    mapLines: [], // 当前展示的所有线数据
    mapPoints: [], // 当前展示的所有点数据
    permission: false, // 权限 false: 可编辑
    astTreeList: [], // 资产设备数据
    drawingState: false, // 是否在绘制状态
    realTimeCoordinates: {}, // 实时坐标
    coordinateShow: false, // 是否显示做标转换
    mapLoad: false, // 是否完成首次加载
    iconEnumObj: undefined,
    highLineData: [], // 高亮显示的线路数据
    highPoints: [], // 高亮显示的点数据
  },
  getters: {
    /**
     * 获取state
     */
    getState(state) {
      return state;
    },
  },
  actions: {},
  mutations: {
    /**
     *  纯函数修改state数据
     *  @params  Object
     */
    setState(state, params) {
      Object.entries(params).forEach(([key, value]) => {
        state[`${key}`] = value;
      });
    },
  },
};

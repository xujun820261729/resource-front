import { CUSTINFORMATION } from '../mutations-type'
// 登录状态
const state = {
  custInformation: {
    isLooks: false,
    custId: '',
    custNo: '',
    flag: '',
    siteNo: '',
    siteId: '',
    siteFalg: '',
    energyUnitId: '',
    type: '',
    typeName: '',
    energyFalg: '',
    isDisabled: false,
    custStatus: ''
  }
}
const actions = {
  getCustInformation ({ state, commit }, flag) {
    commit('CUSTINFORMATION', flag)
  }

}
const mutations = {
  [CUSTINFORMATION] (state, flag) {
    state.custInformation = {
      ...state.custInformation,
      ...flag
    }
    // console.warn(state)
  }

}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}

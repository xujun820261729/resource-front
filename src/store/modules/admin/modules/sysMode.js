export default {
    namespaced: true,
    state: {
        //系统模式
        mode: {
            type: 0,
            data: {
                ai: "",
                appCode: ""
            }
        }
    },
    getters: {
        /**
         * @description 返回当前系统模式
         * @param {*} state vuex state
         */
        mode (state) {
            return state.mode;
        },
    },
    actions: {
    },
    mutations: {
        /**
         * @description 设置系统模式
         * @param {Object} state vuex state
         * @param {Object} info data
         */
        setMode (state, info) {
            state.mode = info;
        },
    }
}

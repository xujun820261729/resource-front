export default {
    namespaced: true,
    state: {
        //cookie信息，绿色国网是iframe内嵌，由于在在跨域的条件下，iframe里的程序是没有办法设置cookie的，所有用store存储
        cookies: {
            pageTabList: "",
            custId: "",
            custName: "",
            uuid:"",
            access_token: ""
        }
    },
    getters: {
        /**
         * @description 返回当前cookie信息
         * @param {*} state vuex state
         */
        cookies (state) {
            return state.cookies;
        },
    },
    actions: {
    },
    mutations: {
        /**
         * @description 设置cookie信息
         * @param {Object} state vuex state
         * @param {Object} info data
         */
        setCookie (state, info) {
            let obj = Object.assign({}, state.cookies, info);
            state.cookies = obj;
        },
    }
}

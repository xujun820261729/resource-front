import system from '@/api/system'
import util from '@/libs/util'
import _ from 'lodash'
// 登录状态
const state = {
  isShow: false,
  inputVal0: null,
  listData: [],
  nodata0: false,
  loading0: true,
  inputVal2: null,
  listData: [],
  latelyData: [],
  nodata2: false,
  loading2: true,
  menuSideThis: null,
}
const mutations = {
  upIsShow(state, isShow) {
    state.isShow = isShow
  },
  upInputVal(state, inputVal) {
    state[`inputVal${inputVal['key']}`] = inputVal['value']
  },
  upNodata(state, nodata) {
    state[`nodata${nodata['key']}`] = nodata['value']
  },
  upLoading(state, loading) {
    state[`loading${loading['key']}`] = loading['value']
  },
  upListData(state, listData) {
    console.log(`${listData['key']}`)
    state[`${listData['key']}`] = listData['value']
  },
  upMenuSideThis(state, that) {
    state.menuSideThis = that
  },
}
const actions = {
  listFavoritedForPaging({ commit }, payload = {}) {
    system.functionFavoriteApi.getFavoriteList({
      userId: util.storage.get('userInfo').id,
      functionName: payload.functionName
    }).then(res => {
      let data = _.get(res, 'result.sysFunctionFavoriteVOS', [])
      if (res.code == 200) {
        commit('upListData', { key: 'listData', value: data })
      } else {
        commit('upListData', { key: 'listData', value: [] })
      }
      commit('upNodata', { key: 0, value: data.length == 0 })
      commit('upLoading', { key: 0, value: false })
    })
  },
  listRecentlyUsedForPaging({ commit }, payload = {}) {
    return
    system.functionFavoriteApi.getFunctionUserUsedList({
      userId: util.storage.get('userInfo').id,
      functionName: payload.functionName,
      pageSize: 50,
      pageIndex: 1
    }).then((res) => {
      let data = _.get(res, 'result.customPage.content', [])
      if (res.code == 200) {
        commit('upListData', { key: 'latelyData', value: data })
      } else {
        commit('upListData', { key: 'latelyData', value: [] })
      }
      commit('upNodata', { key: 2, value: data.length == 0 })
      commit('upLoading', { key: 2, value: false })
    })
  },
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
}

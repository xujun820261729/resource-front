/**
 * 用户信息
 * */
export default {
    namespaced: true,
    state: {
        /**
         * 账号类型：是否为能源公司或者客户
         * isGroup为省综合能源公司
         * isPrefecturalGroup为地市综合能源公司
         * isService 为能源服务商
         * isClique 为集团客户
         * isCust 为单体客户
         */
        acountType: "",
        userInfo:{},//20210421 新增，后续废弃info
        custInfo:{}
    },
    getters:{
    },
    mutations:{
        setUserInfo(state,userInfo){
            state.userInfo = userInfo;
        },
        setCustInfo(state,custInfo){
            state.custInfo = custInfo;
        },
    }
}

/**
 * 全景组件化
 * */

export default {
    namespaced: true,
    state: {
        config: {
        }
    },
    mutations: {
        configSet(state, payload) {
          state.config = JSON.parse(JSON.stringify(payload));
        }
    }
};
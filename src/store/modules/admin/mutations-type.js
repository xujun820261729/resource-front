// 用户是否登录
export const ISLOGINUSER = 'ISLOGINUSER' // 登录
export const NOLOGINUSER = 'NOLOGINUSER' // 退出
export const FOURLEVELMENU = 'FOURLEVELMENU' // 四级菜单
export const ORGNODATA = 'ORGNODATA' // 单位名称
export const CUSTINFORMATION = 'CUSTINFORMATION' // 信息
export const USERIDFN = 'USERIDFN' // 信息
export const CHECKFIRSTLOGIN = 'CHECKFIRSTLOGIN' // 是否第一次登录
export const GETPERMISSIONLIST = 'GETPERMISSIONLIST' // 按钮权限
export const GETUERINFO = 'GETUERINFO' // 用户信息

//设置蓝色背景
const state = {
    showMessageModal: false,//是否显示消息弹窗
}
//get
const getters = {
    //获取
    getShowMessageModal: state => {
        return state.showMessageModal
    },
}
//set
const mutations = {
    setShowMessageModal: (state, flag) => {
        state.showMessageModal = flag
    },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations
}

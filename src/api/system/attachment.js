import http from "@/libs/http";
import { ModuleBasePath } from './index';
const isPord = false
//process.env.NODE_ENV === "production";
import util from '@/libs/util'

export default class attachmentApi {
    /**
     * 保存附件信息
     * @param attachmentExtension 附件格式
     * @param attachmentName 	附件名称
     * @param remoteId 业务中台远程id
     * 
     * */
    static saveFileInfo(params) {
        return http.post(ModuleBasePath + '/attachment/saveFileInfo', params);
    }
    /**
     * 获取附件信息
     * @param attachmentId 附件id
     * 
     * */
    static getFileInfo(params) {
        return http.post(ModuleBasePath + '/attachment/getFileInfo', params);
    }

    /**
    * 上传接口
    * @params file 文件对象
    * @return Promise<Boolean>
    * 
    */
    static conmonUpload(params) {

        return new Promise((resolve, reject) => {
            const { file } = params;
            // 文件完整性校验码
            const fileValidateCode = new SparkMD5().append(file).end();
            const fileList = file.name.split('.');
            // 文件完整性校验码
            const fileType = fileList[fileList.length - 1];
            const formData = new FormData();
            formData.append('fileValidateCode', fileValidateCode);
            formData.append('fileName', file.name)
            formData.append('file', file)
            formData.append('fileType', fileType)
            formData.append('businessName', 'pms保电应用')
            //  上传到服务器
            http.upload(`${isPord ? '/pms-pro' : '/egw-zs'}/baseCenter/fileService/upload`, formData).then(res => {
                if (res.status == "000000") {
                    // 保存信息到本地
                    this.saveFileInfo({
                        suffix: fileType,
                        attachmentName: file.name,
                        remoteId: res.result.fileId
                    }).then(res => {
                        if (res.code == 200) {
                            resolve(res.result)
                        } else {
                            reject(fasle)
                        }

                    })
                } else {
                    console.log('ftpApi http upload erro', res.message);
                    reject(false)
                }

            }).catch(err => {
                console.log('ftpApi http upload catch', err);
            })

        })
    }

    /**
     * 公共的文件下载
     * @params attachmentId 图片ID
     * @isDown 是否主动下载
     * @return Promise<Boolean>
    */
    static commonDown(id, isDown = false) {
        return new Promise((reslove, reject) => {
            // 获取本地的图片信息
            this.getFileInfo({
                attachmentId: id
            }).then(res => {
                if (res.code === 200) {
                    http.post(`${isPord ? '/pms-pro/baseCenter/fileService/download' : '/pms-test/resource-center/common/download'}`, {
                        fileId: res.result.attachment.remoteId
                    }, 'blob').then(blob => {
                        // 匹配图片
                        if (/(png|jpe?g|gif|webp|svg)/.test(res.result.attachment.suffix.toLocaleLowerCase())) {
                            const url = window.URL.createObjectURL(blob)
                            reslove({
                                ...res.result.attachment,
                                url,
                            })
                        }
                        if (isDown) {
                            util.download(blob, res.result.attachment.attachmentName)
                        }

                    })
                } else {
                    reject(false)
                }
            }).catch(err => {
                console.log('system.attachmentApi.getFileInfo catch err' + err);
            })
        })
    }
}

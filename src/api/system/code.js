import http from "@/libs/http";
import { ModuleBasePath } from './index';
export default class codeApi {
    //通过类别代码查询字典表集合
    static getCodeList(params) {
        return http.post(ModuleBasePath + '/code/listCode', params);
    }
    //通过分类代码和字典码值查询字典
    static getCodeBySortIdAndValue(params) {
        return http.post(ModuleBasePath + '/code/getCodeBySortIdAndValue', params);
    }
    //通过字典表id查询字典
    static getCodeById(params) {
        return http.post(ModuleBasePath + '/code/getCodeById', params);
    }
}
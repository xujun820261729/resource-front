import userApi from "./user";

import http from "@/libs/http";

const Setting = require("@/setting.env");

//业务模块请求根路径
export const ModuleBasePath = Setting.api.base;

export default class systemApi {
  //使用加密用户号获取token
  static getToken(params) {
    return http.post(ModuleBasePath + "/user/getToken", params);
  }
  //为大屏获取token
  static getTokenForDP(params) {
    return http.post(ModuleBasePath + "/user/getTokenForDP", params);
  }
  // 查询token是否失效
  static checkToken(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 登陆
  static login(params) {
    return http.post(ModuleBasePath + "/auth/login", params);
  }

  // 菜单
  static getRouters(params) {
    return http.get(ModuleBasePath + "/system/menu/getRouters", params);
  }

  // 获取当前登录人信息
  static getInfo(params) {
    return http.get(ModuleBasePath + "/system/user/getInfo", params);
  }

  // 登出
  static logout(params) {
    return http.delete(ModuleBasePath + "/auth/logout", params);
  }

  // 获取部门树列表
  static deptTree(params) {
    return http.get(ModuleBasePath + "/system/user/deptTree", params);
  }

  //  新增用户
  static addUser(params) {
    return http.post(ModuleBasePath + "/system/user", params);
  }

  //  点 修改用户
  static editUser(params) {
    return http.put(ModuleBasePath + "/system/cust", params);
  }

  //  点 查询用户
  static getUserById(id) {
    return http.get(ModuleBasePath + `/system/cust/${id}`);
  }

  // 获取对应的树
  static roleMenuTreeselect(id) {
    return http.get(ModuleBasePath + `/system/menu/roleMenuTreeselect/${id}`);
  }

  // 获取所有的菜单树数据
  static getAllTreeselect() {
    return http.get(ModuleBasePath + "/system/menu/treeselect");
  }

  // 查询icon列表
  static getIconList(params) {
    return http.get(ModuleBasePath + "/system/icon/list", params);
  }

  // 增加icon
  static addIcon(params) {
    return http.put(ModuleBasePath + "/system/icon", params);
  }

  // 查询树
  static queryMapTree(params) {
    return http.get(ModuleBasePath + "/system/map/tree", params);
  }

  // 获取设备坐标
  static getPointById(params) {
    return http.get(ModuleBasePath + "/system/map/getPointById", params);
  }

  // 剖面使用情况查询
  static getHolesList(params) {
    return http.get(ModuleBasePath + "/system/holes/list", params);
  }

  // 获取电缆管沟使用情况详细信息包含线路信息
  static getHolesById(id) {
    return http.get(ModuleBasePath + `/system/holes/${id}`);
  }

  // 删除孔号
  static deleteHolesById(id) {
    return http.delete(ModuleBasePath + `/system/holes/${id}`);
  }

  // 新增孔号
  static addHoles(params) {
    return http.post(ModuleBasePath + `/system/holes`, params);
  }

  // 编辑孔号
  static editHoles(params) {
    return http.put(ModuleBasePath + `/system/holes`, params);
  }

  // 电缆井剖面左侧电缆段查询
  static getWellSections(id) {
    return http.get(ModuleBasePath + `/system/well/getWellSections/${id}`);
  }

  // 电缆井剖面孔查询
  static getWellSectionHoles(id) {
    return http.get(ModuleBasePath + `/system/well/getWellSectionHoles/${id}`);
  }

  // 编辑电缆井
  static editWell(params) {
    return http.put(ModuleBasePath + `/system/well`, params);
  }

  // 查询电缆井信息
  static getWellById(id) {
    return http.get(ModuleBasePath + `/system/well/${id}`);
  }

  // 查询起始点
  static getStartPoint(params) {
    return http.get(ModuleBasePath + `/system/map/getStartPoint`, params);
  }

  // 获取下一个不可编辑的点
  static getNextPoint(params) {
    return http.get(ModuleBasePath + `/system/map/getNextPoint`, params);
  }

  // 获取接下来的管沟
  static getNextCableDithByStartPoint(params) {
    return http.get(ModuleBasePath + `/system/map/getNextCableDithByStartPoint`, params);
  }

  // 获取中接头列表
  static getListNoPage(params) {
    return http.get(ModuleBasePath + `/system/interface/listNoPage`, params);
  }

  // 电缆段新增
  static addSection(params) {
    return http.post(ModuleBasePath + `/system/section/perforation`, params);
  }

  // 电缆段删除
  static sectionDel(id) {
    return http.delete(ModuleBasePath + `/system/section/${id}`);
  }

  // 获取线的数据
  static getLineNoPage(params) {
    return http.get(ModuleBasePath + `/system/line/listNoPage`, params);
  }

  // 系统文件上传
  static fileUpload({ file, attachmentId }) {
    const formData = new FormData();
    console.log("1111", file.name);
    formData.append("fileName", file.name.split(".")[0]);
    formData.append("file", file);
    if (attachmentId) {
      formData.append("attachmentId", attachmentId);
    }
    return http.upload(ModuleBasePath + `/system/file/upload`, formData);
  }

  // 查询文件
  static getByPackageId(params) {
    return http.get(ModuleBasePath + `/system/file/getByPackageId`, params);
  }

  // 删除文件
  static fileDelete(params) {
    return http.get(ModuleBasePath + `/system/file/delete`, params);
  }

  // 杆塔新增 /system/tower​/saveTower
  static saveTower(params) {
    return http.post(ModuleBasePath + "/system/tower/saveTower", params);
  }

  // 杆塔编辑
  static editTower(params) {
    return http.put(ModuleBasePath + "/system/tower", params);
  }

  // 杆塔查询
  static getTowerInfo(id) {
    return http.get(ModuleBasePath + `/system/tower/${id}`);
  }

  // 查询附件的杆塔等 lon=117.21101025680541&lat=34.253652061260375
  static getNearbyPoint(params) {
    return http.get(ModuleBasePath + "/system/map/getNearbyPoint", params);
  }

  // 中接头查询
  static getInterfaceById(id) {
    return http.get(ModuleBasePath + `/system/interface/${id}`);
  }

  // 中接头编辑
  static editInterface(params) {
    return http.put(ModuleBasePath + "/system/interface", params);
  }
  // 中接头编辑
  static getInterfaceList(params) {
    return http.get(ModuleBasePath + "/system/interface/list", params);
  }

  // 变电站信息
  static getSubStation(id) {
    return http.get(ModuleBasePath + `/system/substation/${id}`);
  }

  // 变电站编辑
  static editSubStation(params) {
    return http.put(ModuleBasePath + `/system/substation`, params);
  }

  // 设备类查询
  static getEqipmentById(id) {
    return http.get(ModuleBasePath + `/system/eqipment/${id}`);
  }

  // 变电站编辑
  static editEqipment(params) {
    return http.put(ModuleBasePath + `/system/eqipment`, params);
  }

  // 电缆井删除
  static wellDel(id) {
    return http.delete(ModuleBasePath + `/system/well/${id}`);
  }

  // 变电站删除
  static substationDel(id) {
    return http.delete(ModuleBasePath + `/system/substation/${id}`);
  }

  // 线路删除
  static lineDel(id) {
    return http.delete(ModuleBasePath + `/system/line/${id}`);
  }

  // 杆塔删除
  static towerDel(id) {
    return http.delete(ModuleBasePath + `/system/tower/${id}`);
  }

  // 删除杆塔线
  static towerLineDel(id) {
    return http.delete(ModuleBasePath + `/system/tower/removeLine/${id}`);
  }

  // 5:站所    10:箱变   13:分支箱    12: 环网柜   设备删除
  static eqipmentDel(id) {
    return http.delete(ModuleBasePath + `/system/eqipment/${id}`);
  }

  // 点用户删除
  static custDel(id) {
    return http.delete(ModuleBasePath + `/system/cust/${id}`);
  }

  // 管沟详情
  static getDitch(id) {
    return http.get(ModuleBasePath + `/system/ditch/${id}`);
  }

  // 管沟删除
  static ditchDel(id) {
    return http.delete(ModuleBasePath + `/system/ditch/${id}`);
  }

  // 管沟编辑
  static editDitch(params) {
    return http.put(ModuleBasePath + "/system/ditch", params);
  }

  // 管沟新增
  static addDitch(params) {
    return http.post(ModuleBasePath + "/system/ditch", params);
  }
  // 所有的线数据
  static getLineByOrgCode(params) {
    return http.get(ModuleBasePath + `/system/map/getLineByOrgCode`, params);
  }

  // 获取组织全部的点
  static getPointByOrgCode(params) {
    return http.get(ModuleBasePath + `/system/map/getPointByOrgCode`, params);
  }

  // 通过经纬度获取周围的点:新增电缆管沟使用
  static getNearbyPointForDitch(params) {
    return http.get(ModuleBasePath + `/system/map/getNearbyPointForDitch`, params);
  }

  // 移动点位置
  static movePoint(params) {
    return http.post(ModuleBasePath + "/system/map/movePoint", params);
  }

  // 移动点
  static holesMove(params) {
    return http.post(ModuleBasePath + "/system/holes/move", params);
  }
}

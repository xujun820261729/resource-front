import http from "@/libs/http";
import { ModuleBasePath } from "./index";
export default class userApi {
  //使用加密用户号获取token
  static getToken(params) {
    return http.post(ModuleBasePath + "/user/getToken", params);
  }
  //为大屏获取token
  static getTokenForDP(params) {
    return http.post(ModuleBasePath + "/user/getTokenForDP", params);
  }
  // 查询token是否失效
  static checkToken(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 更新用户
  static updateUser(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }
  // 用户权保存
  static powerSave(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 菜单权限列表
  static getPowerList(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 图标维护
  static updateChart(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 新增图标
  static addChart(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除图标
  static addChart(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 图标列表
  static queryChart(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  //  新增用户
  static addUser(params) {
    return http.post(ModuleBasePath + "/system/user", params);
  }

  // 获取角色列表
  static getRoleList(params) {
    return http.get(ModuleBasePath + "/system/role/list", params);
  }

  //  用户列表
  static getUserList(params) {
    return http.get(ModuleBasePath + "/system/user/list", params);
  }

  //  删除用户
  static deleteUser(id) {
    return http.delete(ModuleBasePath + `/system/user/${id}`);
  }

  // 重置密码
  static resetPwd(params) {
    return http.put(ModuleBasePath + "/system/user/resetPwd", params);
  }
}

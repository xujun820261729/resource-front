import http from "@/libs/http";
import { ModuleBasePath } from './index';

export default class orginfoApi {
    //通过单位Id查询组织机构号 parentOrgId
    static getTreeOrgInfo(params) {
        return http.post(ModuleBasePath + '/orginfo/getTreeOrgInfo', params);
    }
    //通过单位Id查询组织机构号 comId
    static getOrgInfoByComId(params) {
        return http.post(ModuleBasePath + '/orginfo/getOrgInfoByComId', params);
    }
    //查询树形地市组织机构号
    static getTreeOrgCity(params) {
        return http.post(ModuleBasePath + '/orginfo/getTreeOrgCity', params);
    }

    // 查询树形组织机构号
    static getOrgTreeInfoByOrgId(params) {
        return http.post(ModuleBasePath + '/orginfo/getOrgTreeInfoByOrgId', params);
    }

}

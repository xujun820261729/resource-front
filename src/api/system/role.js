import http from "@/libs/http";
import { ModuleBasePath } from "./index";
export default class roleApi {
  // 角色列表
  static getRoleList(params) {
    return http.get(ModuleBasePath + "/system/role/list", params);
  }

  // add角色
  static addRole(params) {
    return http.post(ModuleBasePath + "/system/role", params);
  }

  // delete 角色
  static deleteRole(id) {
    return http.delete(ModuleBasePath + `/system/role/${id}`);
  }
}

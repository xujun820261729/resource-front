import http from "@/libs/http";

const Setting = require("@/setting.env");
import util from "@/libs/util";

//业务模块请求根路径
export const ModuleBasePath = Setting.api.base;

export default class usherApi {
  // 查询变电站
  static querySubstation(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除变电站
  static delSubstation(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询线路
  static queryLine(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除线路
  static delLine(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询杆塔
  static queryTower(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除杆塔
  static delTower(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询开关
  static querySwitch(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除开关
  static delSwitch(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询站所
  static queryStation(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除站所
  static delStation(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询用户
  static queryUser(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除用户
  static delUser(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询电缆段
  static queryCableSection(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除电缆段
  static delCableSection(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询箱变
  static queryBoxTypeSubstation(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除箱变
  static delBoxTypeSubstation(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询杆上变
  static queryPoleUpChange(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除杆上变
  static delPoleUpChange(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询环网柜
  static queryRmu(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除环网柜
  static delRmu(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询分支箱
  static queryBranchBox(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除分支箱
  static delBranchBox(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 查询电缆井
  static queryCableWell(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 删除电缆井
  static delCableWell(params) {
    return http.post(ModuleBasePath + "/user/getTokenIsExist", params);
  }

  // 变电站下载模版
  static substationImportTemplate(params) {
    http.post(ModuleBasePath + "/system/substation/importTemplate", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "变电站导入模版.xls");
    });
  }

  // 变电站导入
  static substationImportData(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/substation/importData", formData);
  }

  // 线路下载模版
  static lineImportTemplate(params) {
    http.post(ModuleBasePath + "/system/line/importTemplate", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "线路导入模版.xls");
    });
  }

  // 查询列表 type 1:变电站导入； 2：线路； 3:杆塔； 4：开关 5:站所 6:用户 7:电缆断 8:箱变 9:杆上 10:环网柜 11:分支箱
  static searchList(params) {
    return http.get(ModuleBasePath + "/system/log/list", params);
  }

  // 线路导入
  static lineImportData(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/line/importData", formData);
  }

  // 查询所有变电站
  static getAllSubstation(params) {
    return http.get(ModuleBasePath + "/system/substation/listNoPage", params);
  }

  // 下载导入模板分支箱
  static importTemplateFZX(params) {
    http.post(ModuleBasePath + "/system/eqipment/importTemplateFZX", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "分支箱导入模版.xls");
    });
  }

  // 导入数据分支箱
  static importDataFZX(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/eqipment/importDataFZX", formData);
  }

  // 下载导入模板环网柜
  static importTemplateHWG(params) {
    http.post(ModuleBasePath + "/system/eqipment/importTemplateHWG", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "环网柜导入模版.xls");
    });
  }

  // 导入数据环网相
  static importDataHWG(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/eqipment/importDataHWG", formData);
  }

  // 下载导入模板箱变
  static importDataXB(params) {
    http.post(ModuleBasePath + "/system/eqipment/importDataXB", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "箱变导入模版.xls");
    });
  }

  // 下载导入模板箱变
  static importTemplateXB(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/eqipment/importTemplateXB", formData);
  }

  // 下载导入模板站房
  static importTemplatezF(params) {
    http.post(ModuleBasePath + "/system/eqipment/importTemplateZF", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "站房导入模版.xls");
    });
  }

  // 下载导入模板站房
  static importDatazF(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/eqipment/importDataZF", formData);
  }

  // 电缆井导入
  static importTemplateWell(params) {
    http.post(ModuleBasePath + "/system/well/importTemplate", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "电缆井导入模版.xls");
    });
  }

  // 下载导入 电缆井
  static importDataWell(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/well/importData", formData);
  }

  // 电缆段 导入
  static importTemplateDitch(params) {
    http.post(ModuleBasePath + "/system/ditch/importTemplate", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "电缆段导入模版.xls");
    });
  }

  // 下载导入 电缆段
  static importDataDitch(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/ditch/importData", formData);
  }

  // 用户 导入
  static importTemplateCust(params) {
    http.post(ModuleBasePath + "/system/cust/importTemplate", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "用户导入模版.xls");
    });
  }

  // 下载导入 用户
  static importDataCust(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/cust/importData", formData);
  }

  // 杆塔 导入
  static importTemplateTower(params) {
    http.post(ModuleBasePath + "/system/tower/importTemplate", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "杆塔导入模版.xls");
    });
  }

  // 下载导入 杆塔
  static importDataTower(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/tower/importData", formData);
  }

  // 下载导入 开闭所
  static importTemplateKBS(params) {
    http.post(ModuleBasePath + "/system/eqipment/importTemplateKBS", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "开闭所导入模版.xls");
    });
  }

  // 下载导入 开闭所
  static importDataKBS(file) {
    const formData = new FormData();
    formData.append("fileName", file.name);
    formData.append("file", file);
    return http.upload(ModuleBasePath + "/system/eqipment/importDataKBS", formData);
  }
}

import http from "@/libs/http";

const Setting = require("@/setting.env");
import util from "@/libs/util";

//业务模块请求根路径
export const ModuleBasePath = Setting.api.base;

export default class reportApi {
  // 查询电缆井列表
  static getWellList(params) {
    return http.get(ModuleBasePath + "/system/well/list", params);
  }

  // 电缆管沟列表
  static getDitchList(params) {
    return http.get(ModuleBasePath + "/system/ditch/list", params);
  }

  // 中接头列表
  static getInterfaceList(params) {
    return http.get(ModuleBasePath + "/system//interface/list", params);
  }

  //电缆段列表
  static getSectionList(params) {
    return http.get(ModuleBasePath + "/system/section/list", params);
  }

  //  导出 电缆段
  static sectionExport(params) {
    http.post(ModuleBasePath + "/system/section/export", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "电缆段.xls");
    });
  }

  //  导出 电缆井
  static wellExport(params) {
    http.post(ModuleBasePath + "/system/well/export", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "电缆井.xls");
    });
  }

  //  导出 中接头
  static interfaceExport(params) {
    http.post(ModuleBasePath + "/system/interface/export", params, "blob").then((blob) => {
      util.download(new Blob([blob]), "中接头.xls");
    });
  }

  //  导出 电缆管沟
  static ditchExport(params) {
    http.post(ModuleBasePath + "/system/ditch/export", params, "blob").then((blob) => {
      console.log("blob", blob);
      util.download(new Blob([blob]), "电缆管沟.xls");
    });
  }
}

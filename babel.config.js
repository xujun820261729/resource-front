module.exports = {
    presets: [
        '@vue/app'
    ],
    plugins: [
        // https://babeljs.io/docs/en/babel-plugin-transform-runtime/
        '@babel/plugin-transform-runtime',
        "transform-flow-comments",
        // process.env.NODE_ENV === "production" ?
        //     ['transform-remove-console', { 'exclude': ['console'] }]
        //     : {}

    ]
}

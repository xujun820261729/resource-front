const { name } = require("./package.json");

const webpack = require("webpack");
const minimist = require("minimist");
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
// 拼接路径
const resolve = (dir) => require("path").join(__dirname, dir);

const args = minimist(process.argv.slice(2));

// 增加环境变量
process.env.VUE_APP_VERSION = require("./package.json").version;

const Setting = require("./src/setting.env");

const isPord = true;
// process.env.NODE_ENV === "production";

module.exports = {
  publicPath: Setting.publicPath,
  lintOnSave: Setting.lintOnSave,
  outputDir: Setting.outputDir,
  assetsDir: Setting.assetsDir,
  runtimeCompiler: true,
  productionSourceMap: true,
  devServer: {
    disableHostCheck: true,
    port: process.env.port || 8081,
    publicPath: Setting.publicPath,
    // 增加浏览器支持跨域请求头
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    proxy: Setting.baseProxy,
    allowedHosts: ["com.cn", ".com.cn"],
  },
  css: {
    loaderOptions: {
      less: {
        prependData: `@import "~@/styles/index.less"`,
      },
    },
    sourceMap: true,
  },

  // transpileDependencies: ['view-design'],
  configureWebpack: (config) => {
    const gzipPlugin = isPord
      ? [
          new CompressionPlugin({
            test: /\.(js|css)(\?.*)?$/i, //需要压缩的文件正则
            threshold: 10 * 1024, //文件大小大于这个值时启用压缩
            deleteOriginalAssets: false, //压缩后保留原文件
          }),
        ]
      : [];
    return {
      plugins: [
        new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          "windows.jQuery": "jquery",
        }),
        ...gzipPlugin,
      ],
      // output: {
      //     library: `${name}-[name]`,
      //     libraryTarget: 'umd',
      //     jsonpFunction: `webpackJsonp_${name}`,
      // }
    };
  },
  // 默认设置: https://github.com/vuejs/vue-cli/tree/dev/packages/@vue/cli-service/lib/config/base.js
  chainWebpack: (config) => {
    /**
     * 删除懒加载模块的 prefetch preload，降低带宽压力
     * https://cli.vuejs.org/zh/guide/html-and-static-assets.html#prefetch
     * https://cli.vuejs.org/zh/guide/html-and-static-assets.html#preload
     * 而且预渲染时生成的 prefetch 标签是 modern 版本的，低版本浏览器是不需要的
     */
    config.module.rules.delete("eslint");
    config.plugins.delete("prefetch").delete("preload");
    // 解决 cli3 热更新失效 https://github.com/vuejs/vue-cli/issues/1559
    config.resolve.symlinks(true);
    config
      // 开发环境
      .when(
        !isPord,
        // sourcemap不包含列信息
        (config) => config.devtool("cheap-module-eval-source-map")
      )
      // 生产环境
      .when(isPord, (config) => {
        config
          .plugin("ScriptExtHtmlWebpackPlugin")
          .after("html")
          .use("script-ext-html-webpack-plugin", [
            {
              inline: /runtime\..*\.js$/,
            },
          ])
          .end();
        config.optimization.runtimeChunk("single");
      });
    // 不编译 iView Pro
    config.module
      .rule("js")
      .test(/\.jsx?$/)
      .exclude.add(resolve("src/libs/iview-pro"))
      .add(resolve("src/libs/hightopo"))
      .end();
    // 使用 iView Loader
    config.module
      .rule("vue")
      .test(/\.vue$/)
      .use("iview-loader")
      .loader("iview-loader")
      .tap(() => {
        return Setting.iviewLoaderOptions;
      })
      .end();
    // markdown
    config.module.rule("md").test(/\.md$/).use("text-loader").loader("text-loader").end();
    // i18n
    config.module
      .rule("i18n")
      .resourceQuery(/blockType=i18n/)
      .use("i18n")
      .loader("@kazupon/vue-i18n-loader")
      .end();
    // image exclude
    const imagesRule = config.module.rule("images");
    imagesRule
      .test(/\.(png|jpe?g|gif|webp|svg)(\?.*)?$/)
      .exclude.add(resolve("src/assets/svg"))
      .end();
    // 重新设置 alias
    config.resolve.alias.set("@api", resolve("src/api"));
    // node
    config.node.set("__dirname", true).set("__filename", true);
    // 判断是否需要加入模拟数据
    const entry = config.entry("app");
    if (Setting.isMock) {
      entry.add("@/mock").end();
    }
  },
};
